

import argparse
import pathlib
import sys
import textwrap

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

try:
    sys.path.insert(1, 'code/')
except ImportError:
    print("is it the correct working directory ?")
    exit(1)
    # %pwd


if __name__ == "__main__":

    # %% help message
    parser = argparse.ArgumentParser(
        description="""
        Produce a boxplot of the given measure.
        It works with with benchmark files produced (for example) by snakemake
        """,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument('--benchmarkfolder',
                        type=pathlib.Path,
                        required=True,
                        help=textwrap.dedent('''\
                        Folder where the benchmark files are stored.
                        ''')
                        )
    # parser.add_argument('--measure',
    #                     type=str,
    #                     required=True,
    #                     help=textwrap.dedent('''\
    #                     Name of the measure to plot. (it will be in abcisse and the # of files processed in ordinate)
    #                     ''')
    #                     )
    # parser.add_argument('--sbmlList',
    #                     type=pathlib.Path,
    #                     required=False,
    #                     help=textwrap.dedent('''\
    #                     Optinal. List of SBML files to take into account.
    #                     Used to subsetet the points.
    #                     If none provided, all the points are plotted.
    #                     If one is provided but no benchmark file is associated, it will exit.
    #                     ''')
    #                     )
    #
    parser.add_argument('--pathout',
                        type=pathlib.Path,
                        required=True,
                        help=textwrap.dedent('''\
                        Path where to save the resulting image
                        ''')
                        )

    # %pwd
    # benchmarkfolder = pathlib.Path(
    #    "/home/nanis/Documents/TRAVAIL/THESE/PROJETS/CMSB2020/experiments/xp-runningexampleCMSB2021/benchmarks/")
    # benchmarkfolder = pathlib.Path(
    #    "/home/nanis/Documents/TRAVAIL/THESE/PROJETS/CMSB2020/results-20210316_CMSB3021_merge_1to10/benchmarks/")
    benchmarkfolder = pathlib.Path(
        "/home/nanis/Documents/TRAVAIL/THESE/PROJETS/CMSB2020/experiments/CMSB2021-results_merge-all_1to10/benchmarks/")
    pathout = "torm_20210331_CMSB2021_boxplot-cputime-hours.png"

    identificationmethods = ["caspots", "our"]  # ["caspots", "our"]

    sbmlList_file = ""
    density = False  # if True : ordinate is PROPORTION ; if False : it is NUMBER

    ###########################################################################
    ###########################################################################

    fig, ax = plt.subplots(figsize=(8, 1))

    # ["caspots", "our"]:
    for positionindex, identificationmethod in enumerate(identificationmethods):
        print(identificationmethod)

        # --------------------------------------------------------------------------
        # Determine the list of benchmark files to process

        if not sbmlList_file:
            # process all the benchmark files of a given identification method.
            benchmark_files = list(benchmarkfolder.glob(
                f"*.identification-{identificationmethod}.benchmark.txt"))
            benchmark_files
        else:
            # process only a subset.
            benchmark_files = []

            # extracted from the sbmlList_file :
            sbmlList = ["a.sbml", "b.sbml"]
            sbmlIDList = ["a", "b"]

            for sbmlID in sbmlIDList:
                benchmark_file = pathlib.Path(
                    f"{sbmlID}.identification-{identificationmethod}.benchmark.txt")

                # if benchmark_file.is_file():
                # add even if it does not exist, the remaining of the code will take care of error
                benchmark_files.append(benchmark_file)

        # --------------------------------------------------------------------------
        COLUMNS = ["s", "h:m:s", "max_rss", "max_vms", "max_uss",
                   "max_pss", "io_in", "io_out", "mean_load", "cpu_time"]
        benchmark_data = []
        for filename in benchmark_files:
            print(filename)
            try:
                df = pd.read_csv(filename, index_col=None, header=0, sep='\t')
                df["benchmark_file"] = filename

            except pd.errors.EmptyDataError:
                df = pd.DataFrame.from_dict({"benchmark_file": [filename]})
                for col in COLUMNS:
                    df[col] = np.inf

            # print(df)
            benchmark_data.append(df)

        benchmark_df = pd.concat(
            benchmark_data, axis=0, ignore_index=True, sort=True)
        # benchmark_df = pd.DataFrame(benchmark_data, columns=)
        benchmark_df.head()

        # conversion from seconds to hours :
        x = benchmark_df.cpu_time / 60 / 60
        print(x.min(), x.max())

        ax.boxplot(
            x,
            vert=False,
            labels=[identificationmethod],
            positions=[positionindex]
        )

    ax.axvline(x=30, color='white', linewidth=2)
    ax.axvline(x=30, color='black', linewidth=2, linestyle='--')

    ax.grid(False)
    # ax.legend(
    #     # loc='lower right',
    #     bbox_to_anchor=(1.4, 0.5))
    # ax.set_title('Boxplot')
    # ax.set_xlabel('CPU time (hours)')
    # plt.xscale("log")
    # ax.set_ylabel('# models processed')
    # ax.grid(False)
    # ax.legend(
    #     loc='center left',
    #     bbox_to_anchor=(1, 0.5))
    # ax.set_xlabel('CPU time (hours)')
    # # plt.xscale("log")
    # ax.set_ylabel('# models processed')

    ###########################################################################

    # Save plot :
    plt.savefig(pathout, bbox_inches='tight')
