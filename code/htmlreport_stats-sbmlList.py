import argparse
import pathlib
import re
import sys
from collections import Counter

import libsbml

import matplotlib.pyplot as plt

try:
    sys.path.insert(1, 'code/')
    from reports import Report

except ImportError:
    print("is it the correct working directory ?")
    exit(1)
    # %pwd


if __name__ == '__main__':

    parser = argparse.ArgumentParser(
        description='Description', formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--sbmlList',
                        help='Path to the file listing the BIOMDS sbml files taken into account',
                        type=str,
                        required=True
                        )
    parser.add_argument('--outfolderpath',
                        help='Path to the folder where the report have to be saved. The images will be it the subfolder "img"',
                        type=str,
                        required=True
                        )

    # sys.argv = ["--sbmlList", "FROM_CLUSTER/20201011_xp_minMAE_minInputs/sbml-list.txt", "--outfolderpath", "report-curation/"]
    # args = parser.parse_args( sys.argv)
    args = parser.parse_args()

    args.outfolderpath = pathlib.Path(args.outfolderpath)
    (args.outfolderpath / "img").mkdir(parents=True, exist_ok=True)

    rep = Report()
    rep.add_title(f"Some stats about the sbml files from {args.sbmlList} ")

    pattern = re.compile("BIOMD\d{10}")

    sbml_paths = []

    sbml_levels = []
    sbml_version = []
    sbml_errors = []

    sbml_nbspecies = []
    sbml_nbparams = []
    sbml_nbreactions = []

    libsbmlreader = libsbml.SBMLReader()
    with open(args.sbmlList, "r") as f:
        for line in f:

            print('----------')
            print(line)
            # each line is the path of a BIOMD file.
            sbml_path = line.strip()
            rep.add_markdown(sbml_path)

            # get the BIOD_id
            result = pattern.search(sbml_path)
            BIOMD_id = result.group(0)
            rep.add_markdown(BIOMD_id)

            sbml_paths.append(sbml_path)

            # ----------
            # get infos:

            document = libsbmlreader.readSBMLFromFile(sbml_path)

            sbml_levels.append(document.getLevel())
            sbml_version.append(document.getVersion())
            sbml_errors.append(document.getNumErrors())

            model = document.getModel()
            sbml_nbspecies.append(model.getNumSpecies())
            # sbml_nbparams.append()
            # sbml_nbreactions.append()

    # Barplots :
    for name, values in {
        "sbml_levels": sbml_levels,
        "sbml_levelversions": [f"{l}_{v}" for l, v in zip(sbml_levels, sbml_version)],
        "sbml_errors": sbml_errors,
    }.items():
        rep.add_markdown(f"**Distribution {name}**")
        values = map(str, values)
        # Use a Counter to count the number of instances in x
        c = Counter(values)

        plt.bar(c.keys(), c.values())
        rep.add_figure()  # Add the fig that has just been ploted
        plt.close()

    # Histogram :
    for name, values in {
        "sbml_nbspecies": sbml_nbspecies,
        # "sbml_nbparams": sbml_nbparams,
        # "sbml_nbreactions": sbml_nbreactions,
    }.items():
        rep.add_markdown(f"**Distribution {name}**")
        plt.hist(values, bins='auto')
        rep.add_figure()  # Add the fig that has just been ploted
        plt.close()

    rep.write_report(
        filename=args.outfolderpath / f'main.html')
    print("over")
