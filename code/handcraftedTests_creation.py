
import random
import sys

try:
    sys.path.insert(1, 'code/')
    from handcraftedTests_helpers import *
except ImportError:
    print("is it the correct working directory ?")
    # %pwd

random.seed(1)

# %%
##############################################################################
# test False/1:
##############################################################################
dirpath = "data/handcrafted-systems/False/1_Arandom-Xconstant/"

bnets = [
    """\
    NODEA, FALSE
    NODEX, FALSE
    """]
# end first line with \ to avoid the empty line!


# A in the PKN of X
pkn = [
    ["nodea",  1, "nodex"]
]


# simulate values such as :
# A : 10 random value between 0 and 100
# X : striclty contant (68)

nodeA = get_random_intvalues(0, 100, 10)
nodeA

nodeX = [68] * 10
nodeX

ts_data = [nodeA, nodeX]
ts_keys = ["nodea", "nodex"]


text_README = """\
    - A generated randomly between 0 and 100
    - X strictly constant (68)
    """

save_test(dirpath, bnets, pkn, ts_data, ts_keys, text_README)

# %%
##############################################################################
# test False/2:
##############################################################################
dirpath = "data/handcrafted-systems/False/2_Arandom-Xrandom-uncorrelated/"

bnets = [
    """\
    NODEA, FALSE
    NODEX, FALSE
    """]


# A in the PKN of X
pkn = [
    ["nodea", 1, "nodex"]
]

# simulate values such as :
# A : 10 random value between 0 and 100
# X : 10 random value between 0 and 100
# no correlation betwwen A and B

nodeA = get_random_intvalues(0, 100, 10)
nodeA
nodeX = get_uncorrelated_intvalues(0, 100, nodeA)
nodeX

ts_data = [nodeA, nodeX]
ts_keys = ["nodea", "nodex"]


text_README = """\
- a generated randomly between 0 and 100
- x generated randomly between 0 and 100
No correlation between a and x
"""

save_test(dirpath, bnets, pkn, ts_data, ts_keys, text_README)

# %%
##############################################################################
# test False/3:
##############################################################################
dirpath = "data/handcrafted-systems/False/3_Arandom_XfollowsA/"

bnets = [
    """\
    NODEA, FALSE
    NODEX, FALSE
    """]


# A in the PKN of X
pkn = [
    ["nodea", 1, "nodex"]
]


# simulate values such as :
# A : 10 random value between 0 and 100
# X(t) = -A(t-1)

nodeA = get_random_intvalues(0, 100, 10)
nodeA


nodeX = nodeA[-1:] + nodeA[:-1]
nodeX
nodeX = [v * -1 for v in nodeX]
nodeX


ts_data = [nodeA, nodeX]
ts_keys = ["nodea", "nodex"]


text_README = """\
- a generated randomly between 0 and 100
- x_t = -a_{t-1}
"""


save_test(dirpath, bnets, pkn, ts_data, ts_keys, text_README)

# %%
##############################################################################
# test False/4:
##############################################################################
dirpath = "data/handcrafted-systems/False/4_Aincreases-XistheoppositeofA/"

bnets = [
    """\
    NODEA, FALSE
    NODEX, FALSE
    """]


# A in the PKN of X
pkn = [
    ["nodea", 1, "nodex"]
]


# simulate values such as :
# A : 10 random value between 0 and 100
# X(t) = -A(t-1)

nodeA = get_increasing_intvalues(0, 100, 10)
nodeA


nodeX = [0] + [v * -1 for v in nodeA[:-1]]
nodeX


ts_data = [nodeA, nodeX]
ts_keys = ["nodea", "nodex"]


text_README = """\
- a generated randomly between 0 and 100
- x_t = -a_{t-1}
"""


save_test(dirpath, bnets, pkn, ts_data, ts_keys, text_README)
# %%
##############################################################################
# test A/onlyAinPKN/1:
##############################################################################
dirpath = "data/handcrafted-systems/nodeA/onlyAinPKN/1_Aincreasesthendecreases-XfollowsA/"

bnets = [
    """\
    NODEA, FALSE
    NODEX, NODEA
    """]


# both A and -A in the PKN of X
pkn = [
    ["nodea", 1, "nodex"],
    ["nodea", -1, "nodex"]
]


# simulate values such as :

# nodeA =  values (either -100 or 0 or 100) increasing then decreasing
nodeA = [-100, 0, 100, 100, 0, -100, -100]
nodeA

# nodeX_t = nodeA_{t-1}
nodeX = [-100, -100, 0, 100, 100, 0, -100]
nodeX


ts_data = [nodeA, nodeX]
ts_keys = ["nodea", "nodex"]


text_README = """\
- nodeA =  values (either -100 or 0 or 100) increasing then decreasing
- nodeX_t = nodeA_{t-1}
"""


save_test(dirpath, bnets, pkn, ts_data, ts_keys, text_README)
# %%
##############################################################################
# test A/onlyAinPKN/2:
##############################################################################
dirpath = "data/handcrafted-systems/nodeA/onlyAinPKN/2_Aincreases_XfollowsA/"


bnets = [
    """\
    NODEA, FALSE
    NODEX, NODEA
    """]


# both A and -A in the PKN of X
pkn = [
    ["nodea", 1, "nodex"],
    ["nodea", -1, "nodex"]
]


# simulate values such as :

# nodeA =  values (either -100 or 0 or 100) increasing
nodeA = [-100, 0, 100, 100, 100]
nodeA

# nodeX_t = nodeA_{t-1}
nodeX = [-100, -100, 0, 100, 100]
nodeX


ts_data = [nodeA, nodeX]
ts_keys = ["nodea", "nodex"]


text_README = """\
-nodeA =  values (either -100 or 0 or 100) increasing
- nodeX_t = nodeA_{t-1}
"""

save_test(dirpath, bnets, pkn, ts_data, ts_keys, text_README)
# %%
##############################################################################
# test A/onlyAinPKN/3:
##############################################################################
dirpath = "data/handcrafted-systems/nodeA/onlyAinPKN/3_Aincreases_XfollowsA-moretimepoints/"


bnets = [
    """\
    NODEA, FALSE
    NODEX, NODEA
    """]


# both A and -A in the PKN of X
pkn = [
    ["nodea", 1, "nodex"],
    ["nodea", -1, "nodex"]
]


# simulate values such as :
# A_t increasing.
# X_t = f(A_{t-1})
# Compared to test A/2, we just have move time points about the activation of X because of the activation of A.

# nodeA = 10 increasing vales
nodeA = [-100, -100, -100, 0, 100, 100, 100, 100, 100, 100, 100]
nodeA

# nodeX at time T are equal to the values of nodeA at T-1
nodeX = [-100, -100, -100, -100, 0, 100, 100, 100, 100, 100, 100]
nodeX


ts_data = [nodeA, nodeX]
ts_keys = ["nodea", "nodex"]


text_README = """\
- nodeA =  values (either -100 or 0 or 100) increasing
- nodeX_t = nodeA_{t-1}
Compared to the test A/2, we have more datapoint about the activation of X when A is activated.
"""

save_test(dirpath, bnets, pkn, ts_data, ts_keys, text_README)
# %%
##############################################################################
# test A/onlyAinPKN/4:
##############################################################################
dirpath = "data/handcrafted-systems/nodeA/onlyAinPKN/4_Aincreasessmoothly_XfollowsA//"

bnets = [
    """\
    NODEA, FALSE
    NODEX, NODEA
    """]


# both A and -A in the PKN of X
pkn = [
    ["nodea", 1, "nodex"],
    ["nodea", -1, "nodex"]
]


# simulate values such as :

# nodeA = 10 increasing values between -100 and 100. smooth augmentation.
nodeA = [-100, -50, -40, -30, -20, -10, -5, 10, 20, 30, 50, 70, 100, 100]
nodeA

# nodeX at time T equal nodeA at T-1
nodeX = [-100, -100, -50, -40, -30, -20, -10, -5, 10, 20, 30, 50, 70, 100]
nodeX


ts_data = [nodeA, nodeX]
ts_keys = ["nodea", "nodex"]


text_README = """\
- nodeA =  increasing values betweeen -100 and 100
- nodeX_t = nodeA_{t-1}
"""

save_test(dirpath, bnets, pkn, ts_data, ts_keys, text_README)
# %%
##############################################################################
# test A/onlyAinPKN/5:
##############################################################################
dirpath = "data/handcrafted-systems/nodeA/onlyAinPKN/5-Arandomlyincreasing-XfollowsA/"


bnets = [
    """\
    NODEA, FALSE
    NODEX, NODEA
    """]


# both A and -A in the PKN of X
pkn = [
    ["nodea", 1, "nodex"],
    ["nodea", -1, "nodex"]
]


# simulate values such as :

# nodeA = 10 random values between 0 and 100 in increasing order
nodeA = get_increasing_intvalues(0, 100, 10)
nodeA

# nodeX starts from 0 and then its values at time T are equal to the values of nodeA at T-1
nodeX = get_shifted(nodeA, 1)
nodeX


ts_data = [nodeA, nodeX]
ts_keys = ["nodea", "nodex"]


text_README = """\
- nodeA =  randomly generated increasing values between 0 and 100
- X : start at 0 and then nodeX_t = nodeA_{t-1}
"""

save_test(dirpath, bnets, pkn, ts_data, ts_keys, text_README)

# %%
##############################################################################
# test A/onlyAinPKN/6:
#############################################################################

dirpath = "data/handcrafted-systems/nodeA/onlyAinPKN/6_Arandomlyincreasing-XfollowsAwith-error/"


bnets = [
    """\
    NODEA, FALSE
    NODEX, NODEA
    """]


# both A and -A in the PKN of X
pkn = [
    ["nodea", 1, "nodex"],
    ["nodea", -1, "nodex"]
]


# simulate values such as :

# nodeA = 10 random values between 0 and 100 in increasing order
nodeA = get_increasing_intvalues(0, 100, 10)
nodeA

# nodeX starts from 0 and then its values at time T are equal to the values of nodeA at T-1 +- error
nodeX = get_shifted(nodeA, 1)
nodeX
nodeX = noisify(nodeX, 0, 5)  # mean, std
nodeX


ts_data = [nodeA, nodeX]
ts_keys = ["nodea", "nodex"]


text_README = """\
- nodeA =  randomly generated increasing values between 0 and 100
- X : start at 0 and then nodeX_t = nodeA_{t-1} +- error
"""


save_test(dirpath, bnets, pkn, ts_data, ts_keys, text_README)
# %%
##############################################################################
# test A/onlyAinPKN/7: delay = 2 > 1
#############################################################################
delay = 2
dirpath = f"data/handcrafted-systems/nodeA/onlyAinPKN/7_Arandomlyincreasing-XfollowsAwithdelay{delay}/"


bnets = [
    """\
    NODEA, FALSE
    NODEX, NODEA
    """]


# both A and -A in the PKN of X
pkn = [
    ["nodea", 1, "nodex"],
    ["nodea", -1, "nodex"]
]


# simulate values such as :
# A_t increasing
# X_t = f(A_{t-delay )

# nodeA = 10 random values between 0 and 100 in increasing order
nodeA = get_increasing_intvalues(0, 100, 10)
nodeA

# nodeX starts from 0 and then its values at time T are equal to the values of nodeA at T-delay
nodeX = get_shifted(nodeA, delay)
nodeX


ts_data = [nodeA, nodeX]
ts_keys = ["nodea", "nodex"]


text_README = """\
- nodeA =  randomly generated increasing values between 0 and 100
- X : start at 0 and then nodeX_t = nodeA_{t- delay }
"""

save_test(dirpath, bnets, pkn, ts_data, ts_keys, text_README)
# %%
##############################################################################
# test A/onlyAinPKN/8: strict threshold
#############################################################################

dirpath = f"data/handcrafted-systems/nodeA/onlyAinPKN/8_Arandomlyincreasing-XfolowsAwiththreshold/"

bnets = [
    """\
    NODEA, FALSE
    NODEX, NODEA
    """]


# both A and -A in the PKN of X
pkn = [
    ["nodea", 1, "nodex"],
    ["nodea", -1, "nodex"]
]


# simulate values such as :

# nodeA = 10 random values between 0 and 100 in increasing order
nodeA = get_increasing_intvalues(0, 100, 10)
nodeA

# nodeX starts from 0 and then its values at time T are equal to the thresholdified values of nodeA at T-1
nodeX = get_shifted(nodeA, 1)
nodeX
nodeX = thresholdify(nodeX, 50, use_min=True, use_max=True)
nodeX


ts_data = [nodeA, nodeX]
ts_keys = ["nodea", "nodex"]


text_README = """\
- nodeA =  randomly generated increasing values between 0 and 100
- X : start at 0 and then nodeX_t = thersholdified( nodeA_{t-1 } )
"""

save_test(dirpath, bnets, pkn, ts_data, ts_keys, text_README)

# %%
##############################################################################
# test A; A&B in pkn  /case 1:
#############################################################################

dirpath = f"data/handcrafted-systems/nodeA/A&BinPKN/1_Arandomlyincreasing-Brandom-XfollowsA/"


bnets = [
    """\
    NODEA, FALSE
    NODEB, FALSE
    NODEX, NODEA
    """]


# A, -A, B, -B in the PKN of X
pkn = [
    ["nodea", 1, "nodex"],
    ["nodea", -1, "nodex"],
    ["nodeb", 1, "nodex"],
    ["nodeb", -1, "nodex"]

]


# simulate values such as :

# nodeA = 10 random values between 0 and 100 in increasing order
nodeA = get_increasing_intvalues(0, 100, 10)
nodeA

# nodeX starts from 0 and then its values at time T are equal to values of nodeA at T-1
nodeX = get_shifted(nodeA, 1)
nodeX

# nodeB = 10 values completely random between 0 and 100
# uncorrelated with A
nodeB = get_uncorrelated_intvalues(0, 100, nodeA)
nodeB


ts_data = [nodeA, nodeB, nodeX]
ts_keys = ["nodea", "nodeb", "nodex"]


text_README = """\
- nodeA =  randomly generated increasing values between 0 and 100
- nodeB : completely random between 0 and 100
- X : start at 0 and then nodeX_t = nodeA_{t-1 }
"""

save_test(dirpath, bnets, pkn, ts_data, ts_keys, text_README)
# %%
##############################################################################
# test A; A&B in pkn  /case 2 : Both A and B inscrease, but X_t = A_{t-1}:
#############################################################################

dirpath = f"data/handcrafted-systems/nodeA/A&BinPKN/2_Arandomlyincreasing-Brandomlyincreasing-XfollowsA/"


bnets = [
    """\
    NODEA, FALSE
    NODEB, FALSE
    NODEX, NODEA
    """]


# A, -A, B, -B in the PKN of X
pkn = [
    ["nodea", 1, "nodex"],
    ["nodea", -1, "nodex"],
    ["nodeb", 1, "nodex"],
    ["nodeb", -1, "nodex"]

]


# simulate values such as :

# nodeA = 10 random values between 0 and 100 in increasing order
nodeA = get_increasing_intvalues(0, 100, 10)
nodeA

# nodeB = 10 random values between 0 and 100 in increasing order
nodeB = get_increasing_intvalues(0, 100, 10)
nodeB

# nodeX starts from 0 and then its values at time T are equal to values of nodeA at T-1
nodeX = get_shifted(nodeA, 1)
nodeX

ts_data = [nodeA, nodeB, nodeX]
ts_keys = ["nodea", "nodeb", "nodex"]


text_README = """\
- nodeA =  randomly generated increasing values between 0 and 100
- nodeB : completely random between 0 and 100
- X : start at 0 and then nodeX_t = nodeA_{t-1 }
"""

save_test(dirpath, bnets, pkn, ts_data, ts_keys, text_README)

# %%
##############################################################################
# test A; A&B in pkn  /case 3 : A increases, B = A +- error, X_t = A_{t-1}:
#############################################################################

dirpath = f"data/handcrafted-systems/nodeA/A&BinPKN/3_Arandomlyincreasing-BisnoisifiedA-XfollowsA/"


bnets = [
    """\
    NODEA, FALSE
    NODEB, FALSE
    NODEX, NODEA
    """]


# A, -A, B, -B in the PKN of X
pkn = [
    ["nodea", 1, "nodex"],
    ["nodea", -1, "nodex"],
    ["nodeb", 1, "nodex"],
    ["nodeb", -1, "nodex"]

]


# simulate values such as :

# nodeA = 10 random values between 0 and 100 in increasing order
nodeA = get_increasing_intvalues(0, 100, 10)
nodeA

# nodeB = node A +- error
nodeB = noisify(nodeA, 0, 3)
nodeB

# nodeX starts from 0 and then its values at time T are equal to values of nodeA at T-1
nodeX = get_shifted(nodeA, 1)
nodeX


ts_data = [nodeA, nodeB, nodeX]
ts_keys = ["nodea", "nodeb", "nodex"]


text_README = """\
- nodeA =  randomly generated increasing values between 0 and 100
- nodeB = nodeA +- error
- X : start at 0 and then nodeX_t = nodeA_{t-1 }
"""
save_test(dirpath, bnets, pkn, ts_data, ts_keys, text_README)
# %%
##############################################################################
# test A; A&B in pkn  /case 4 : A increases, B(t) = A(t-1) +- error, X(t) = A(t-1) +- error:
#############################################################################

dirpath = f"data/handcrafted-systems/nodeA/A&BinPKN/4_Arandomlyincreasing-BfollowsAwitherror-XfollowsAwitherror/"


bnets = [
    """\
    NODEA, FALSE
    NODEB, FALSE
    NODEX, NODEA
    """]


# A, -A, B, -B in the PKN of X
pkn = [
    ["nodea", 1, "nodex"],
    ["nodea", -1, "nodex"],
    ["nodeb", 1, "nodex"],
    ["nodeb", -1, "nodex"]

]


# simulate values such as :

# nodeA = 10 random values between 0 and 100 in increasing order
nodeA = get_increasing_intvalues(0, 100, 10)
nodeA

# nodeB(t) = node A(t-1) +- error
nodeB = get_shifted(nodeA, 1)
nodeB = noisify(nodeB, 0, 3)
nodeB

# nodeX(t) = nodeA(t-1) +- error
nodeX = get_shifted(nodeA, 1)
nodeX = noisify(nodeX, 0, 3)
nodeX


ts_data = [nodeA, nodeB, nodeX]
ts_keys = ["nodea", "nodeb", "nodex"]


text_README = """\
- nodeA =  randomly generated increasing values between 0 and 100
- nodeB(t) = A(t-1) +- error
- X(t) = A(t-1) +- error
"""
save_test(dirpath, bnets, pkn, ts_data, ts_keys, text_README)

# %%
##############################################################################
# test Akutsu:
##############################################################################
dirpath = "data/handcrafted-systems/Akutsu"

bnets = [
    """\
    NODE1, NODE3
    NODE2, NODE2 & !NODE3
    NODE3, !NODE3
    """,
    """\
    NODE1, NODE3
    NODE2, (NODE2 & !NODE3) | (!NODE2 & NODE3)
    NODE3, NODE1 | !NODE3
    """
]
# end first line with \ to avoid the empty line!


# A in the PKN of X
pkn = [
    ["node3", 1, "node1"],
    ["node2", 1, "node2"],
    ["node2", -1, "node2"],
    ["node3", 1, "node2"],
    ["node3", -1, "node2"],
    ["node1", 1, "node3"],
    ["node2", -1, "node3"],
    ["node3", -1, "node3"]
]


# ts :

node1 = [0, 0, 1, 0]
node1

node2 = [1, 1, 0, 0]
node2

node3 = [0, 1, 0, 1]
node3

ts_data = [node1, node2, node3]
ts_keys = ["node1", "node2", "node3"]


text_README = """\
    Example from Akutsu
    """

save_test(dirpath, bnets, pkn, ts_data, ts_keys, text_README)
