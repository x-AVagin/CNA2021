import argparse
import sys
import textwrap

import networkx as nx
import numpy as np
import pandas as pd
import pyboolnet
from pyboolnet.file_exchange import bnet2primes
from pyboolnet.state_transition_graphs import primes2stg, successors_mixed
from pyboolnet.state_space import state2str

import mpbn

try:
    sys.path.insert(1, 'code/')
    import tshandler
except ImportError:
    print("is it the correct working directory ?")
    # %pwd


def configstr2configmpbn(primes, configuration):
    """

    Arguments:
    ----------
    - primes : dict obtained from pyboolnet.FileExchange.bnet2primes(bnetfile)
    - configuration : string = 0 / 1 vector. One bit for each agent sorted by name

    Return:
    -------
    A configuration usable in mpbn.
    It correspond to a dict.
    keys are agent name.
    value is an int corresponding to the state (0/1) of the agent in the given configuration.

    """

    assert(len(primes) == len(configuration))

    to_ret = dict()
    for prime, state in zip(sorted(primes), configuration):
        to_ret[prime] = int(state)

    return to_ret


if __name__ == "__main__":

    # %% parse arguments
    parser = argparse.ArgumentParser(
        description='Description', formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--bnetfile',
                        type=str,
                        required=True,
                        help=textwrap.dedent('''\
                        bnet file containing the BN of which we want to find the STG
                        ''')
                        )
    parser.add_argument('--binarizedtscsv',
                        type=str,
                        required=False,
                        help=textwrap.dedent('''\
                        Optinal. csv of the binaried ts. If given, a path about the configurations is added in the sif. The edge type is "obs"
                        ''')
                        )

    parser.add_argument('--updatescheme',
                        type=str,
                        required=True,
                        choices=[
                            'synchronous',
                            'asynchronous',
                            'mixed',
                            'mergedsyncasync',
                            'mp',
                            'all'
                        ],
                        help=textwrap.dedent('''\
                        Update scheme that sould be used for the computation of the coverage
                        ''')
                        )

    parser.add_argument('--outputfile',
                        type=str,
                        required=True,
                        help=textwrap.dedent('''\
                        path where the coverage file has to be saved
                        ''')
                        )

    # %% Retrieve arguments
    args = parser.parse_args()
    # sys.argv = ["--bnetfile", "FROM_CLUSTER/20201011_xp_minMAE_minInputs/bnet/BIOMD0000000116/1.bnet", "--binarizedtscsv", "test_ts_binarized.csv", "--outputfile", "test_coverage.txt"]
    # args = parser.parse_args(sys.argv)
    args.bnetfile
    args.binarizedtscsv
    args.outputfile
    args.updatescheme = "mixed"

    # ----------
    # binarited ts to nx graph

    bts = tshandler.tsfile2pddf(args.binarizedtscsv)

    # do not remove consecutive duplicated lines
    # to not miss the self loops for the computation of the coverage.

    # assert columns are sorted in alphabetical order
    # so it matches the order of the agent in the configuration vectors in the stg.
    bts = bts.reindex(sorted(bts.columns), axis=1)
    # 0 is encoded by -1  in the binarized ts.
    # but pyboolnet and mpbn need zeros.
    bts.replace(-1, 0, inplace=True)

    # extract the state sequence as a list of string.
    # each string being a configuration
    obs = [''.join(map(str, row)) for row in bts.values]

    # If there is only one configuration o in the list obs,
    # perhaps the system was in its a fixed points.
    # Hence, add a seconde occurence of o to obs
    # in order to be able to see if there is a "transition" between o and itself
    # -> a self loop
    if len(obs) == 1:
        obs.extend(obs)

    # We now hove to check how many edges (o, o_) exist in the stgs :
    # for o, o_ in zip(obs, obs[1:]):
    #    print(o, o_)

    # ---
    coverage = {
        "mixed": {
            "nb_config_focused": 0,
            "nb_edges_focused": 0,
            "retreived_edges__number": 0,
            "retreived_edges__proportion": 0, }
    }

    # - retreived_edges__number is the number of covered transition
    # - retreived_edges__proportion is the proportion of edge observed in the binarized ts that is also found in the stg obtained with a given semantic
    # = number of covered transition  / number of transition observed
    # At the begining, retreived_edges__proportion is 0 for all update scheme
    # - retrieved_paths__number
    # - retreived_paths__proportion is the proportion of edge observed in the binarized ts
    # among which there is a possible path in the stg obtained with a given semantic
    # At the begining, retreived_paths__proportion is 0 for all update scheme
    # - for the semantic mp, only the proportion of transition o, o'
    # such that o' is reachable from o
    # - Note that number of direct transitions observed is at maximal equal to len(obs) - 1
    transitionDirect_DONE = set()
    for o, o_ in zip(obs, obs[1:]):
        if o == o_ or (o, o_) in transitionDirect_DONE:
            continue
        else:
            transitionDirect_DONE.add((o, o_))
    if len(transitionDirect_DONE) == 0:
        print(
            " !!! no transitionDirect -> It seems all the components were constant for all the TS")

    coverage["obs"] = {
        "nb_config_full": len(obs),
        "nb_config_focused": len(obs),
        "retreived_edges__number": len(transitionDirect_DONE) if len(transitionDirect_DONE) > 0 else np.nan
    }

    print(f"""
          {len(obs)} configurations were observed in the binarized ts,
          {len(transitionDirect_DONE)} transitions observed.
          """)

    # ---
    # Load the bnet file :
    primes = bnet2primes(args.bnetfile)

    # updatescheme = "asynchronous"
    # for updatescheme in ("synchronous", "asynchronous", "mixed"):
    # for updatescheme in ("mixed",):
    if 1:
        updatescheme = "mixed"

        # direct transition
        print("direct transition")
        transitionDirect_DONE = set()  # need this var later to compute the coverage
        # could get rid of it but dont want to do it now.

        nb_transitionDirect_retrieved = 0
        for o, o_ in zip(obs, obs[1:]):

            if o == o_ or (o, o_) in transitionDirect_DONE:
                continue
            else:
                transitionDirect_DONE.add((o, o_))
            print("##########")
            print("edge", o, o_)

            # is o, o_ in the list of mixed successor of o ?
            succ_listofdict = successors_mixed(primes, o)
            succ_listofstr = [state2str(d) for d in succ_listofdict]
            # print(succ_listofstr)
            if o_ in succ_listofstr:
                coverage[updatescheme]["retreived_edges__number"] += 1
                # nb_transitionDirect_retrieved += 1
                print("is there")
        if len(transitionDirect_DONE) > 0:
            denominator = len(transitionDirect_DONE)
            print(coverage[updatescheme]["retreived_edges__number"])
            print(denominator)
            coverage[updatescheme]["retreived_edges__proportion"] = coverage[updatescheme]["retreived_edges__number"] / denominator
            print(coverage[updatescheme]["retreived_edges__proportion"])
        else:  # if they were no transition to retreive, all the methods which returned a BN gets 1:
            coverage[updatescheme]["retreived_edges__proportion"] = 1

    coverage

    coverage_table = pd.DataFrame(coverage)
    coverage_table

    coverage_table.to_csv(args.outputfile)
    print(f"coverage results have been save in {args.outputfile}")
