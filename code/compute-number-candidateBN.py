import argparse
import collections
import pathlib

if __name__ == "__main__":

    # %% help message
    parser = argparse.ArgumentParser(
        description="""
            Tells how many BN candidate they are from a given PKN file
            """,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument('--pknfile',
                        type=pathlib.Path,
                        required=True,
                        help=textwrap.dedent('''\
                            PKN in sif format
                            ''')
                        )

    pkn_structure = collections.defaultdict(set)
    # parent
    # pkn_structure[child].add((parent, sign))
    # f =  open("experiments/xp_OLA21_soumission-paper-v1_SAVE/pkn/arellano_rootstem_asynchronous_1_noise0.1_seed0.sif", "r")
    with args.pknfile.open() as f:
        for line in f:
            print(line)
            # parent sign child
            parent, sign, child = line.split()
            pkn_structure[child].add((parent, sign))


pkn_structure

candidatesPerNode_nosigns = dict()  # no signs -> REVEAL and BestFit
candidatesPerNode_signs_all = dict()  # our
candidatesPerNode_signs_lm = dict()  # signs + local monotonous -> caspots

for child in pkn_structure:
    print(child)
    subpkn = collections.defaultdict(set)
    for parent, sign in pkn_structure[child]:
        subpkn[parent].add(sign)
    # no signs :
    candidatesPerNode_nosigns[child] = 2**len(subpkn)
    # our :
    candidatesPerNode_signs_all[child] = None
    # caspots :
    candidatesPerNode_signs_lm[child] = None

# nodes without parents have 2 candidates : either True, or False.
