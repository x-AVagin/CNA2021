import argparse
import sys
import textwrap

try:
    sys.path.insert(1, 'code/')
    import ts2plots
    import tshandler
except ImportError:
    print("is it the correct working directory ?")
    # %pwd


if __name__ == "__main__":

    # %% help message
    parser = argparse.ArgumentParser(
        description="""
        Produces a stretched version of the given ts.
        At each time step, the agents of the system have a stretched value V ranging from -100 to 100.
        We use minmaxscaler to archive this.
        """,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument('--stsfilepath',
                        type=str,
                        required=True,
                        help=textwrap.dedent('''\
                        csv file storing a stretched ts
                        ''')
                        )
    parser.add_argument('--outfilepath',
                        type=str,
                        required=True,
                        help=textwrap.dedent('''\
                        path to where the csv file containing the binarized ts has to be written
                        ''')
                        )

    args = parser.parse_args()

    # -----------
    s_ts = tshandler.tsfile2pddf(args.stsfilepath)

    b_ts = tshandler.binarization(s_ts, treasold=0)

    b_ts.to_csv(args.outfilepath)
