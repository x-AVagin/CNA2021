import argparse
import sys
import textwrap

import networkx as nx
import numpy as np
import PyBoolNet

try:
    sys.path.insert(1, 'code/')
    import tshandler
except ImportError:
    print("is it the correct working directory ?")
    # %pwd


# PyBoolNet.Tests.Dependencies.run()


def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


if __name__ == "__main__":

    # %% help message
    parser = argparse.ArgumentParser(
        description="""
        Produce a sif file storing the stg graph of the BN encoded in the given bnet file.
        Also, add a path about the binary state sequence observed in the binarized ts.
        """,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument('--bnetfile',
                        type=str,
                        required=True,
                        help=textwrap.dedent('''\
                        bnet file containing the BN of which we want to find the STG
                        ''')
                        )
    parser.add_argument('--binarizedtscsv',
                        type=str,
                        required=False,
                        help=textwrap.dedent('''\
                        Optinal. csv of the binaried ts. If given, a path about the configurations is added in the sif. The edge type is "obs"
                        ''')
                        )

    parser.add_argument('--focusedbts',
                        type=str2bool,
                        nargs='?',
                        const=True,
                        default=False,
                        required=False,
                        help=textwrap.dedent('''\
                        If True, the stage transition graph is computed only starting from the configurations observed in the given bts
                        ''')
                        )

    parser.add_argument('--outputfile',
                        type=str,
                        required=True,
                        help=textwrap.dedent('''\
                        path of the sif file in wich the stg graph will be save
                        ''')
                        )

    parser.add_argument("--updatescheme",
                        type=str,
                        choices=("synchronous", "asynchronous", "mixed"),
                        required=True,
                        help=textwrap.dedent('''\
                        update sheme = in which order should the agent of the BN be updated
                        ''')
                        )

    # %% Retrieve arguments
    args = parser.parse_args()
    # sys.argv = ["--bnetfile", "FROM_CLUSTER/20201011_xp_minMAE_minInputs/bnet/BIOMD0000000116/1.bnet", "--binarizedtscsv", "test_ts_binarized.csv", "--outputfile", "test_stg.sif", "--updatescheme", "synchronous"]
    # args = parser.parse_args(sys.argv)
    args.bnetfile
    args.binarizedtscsv
    args.updatescheme

    # Load the bnet file :
    primes = PyBoolNet.FileExchange.bnet2primes(args.bnetfile)
    primes

    # get binary sequence observed in the binary ts :
    obs = []
    if (args.binarizedtscsv):

        # binarized ts
        bts = tshandler.tsfile2pddf(args.binarizedtscsv)

        # do not remove consecutive duplicated lines
        # to not miss the self loops

        # assert columns are sorted in alphabetical order
        # so it matches the order of the agent in the configuration vectors in the stg.
        bts = bts.reindex(sorted(bts.columns), axis=1)

        # extract the state sequence as a list of string.
        # each string being a configuration

        obs = [''.join(map(str, row)) for row in bts.values]

    # STG : Get the  State Transition Graph

    # by default, compute the compute stg.
    # Except if it has been required to focus on the configuration observed in the ts.
    if args.focusedbts:
        # 0 is encoded by -1  in the binarized ts.
        # but pyboolnet needs zeros.
        ini = [o.replace("-1", "0") for o in obs]
    else:
        def ini(x): return True

    print(primes)
    print(ini)

    # TODO : wait for my PR to be merged, and primes2stg will return an empty nx.Graph when no primes are given :
    if len(primes) == 0:
        stg = nx.DiGraph()
    else:
        stg = PyBoolNet.StateTransitionGraphs.primes2stg(
            primes,
            args.updatescheme,
            InitialStates=ini
        )

    # number of nodes in the graph :
    print(f"{stg.order()} configuration in the stg")

    # nodes of the stg are system configurations where the order is "sorted(Primes)"
    # stg.nodes()

    print(
        f"""
        Each node is a configuration of the system.
        The corresponding order of the system agents is sorted(agentnames):
        {sorted(primes)}.
        """
    )

    # ----------
    # Save stg and obs in the sif file
    with open(args.outputfile, "w") as fsif:
        for st in stg.edges:
            fsif.write(f"{st[0]} stg {st[1]}\n")
        if obs:
            # use a set to not write several time the same line
            # which would end up in ploting several time the edge in the stg.
            # A cool thing would be to modulate the width of the edge depending on how much time the transition was orbeserved.
            for state, stateprime in set(zip(obs[:-1], obs[1:])):
                fsif.write(f"{state} obs {stateprime}\n")
