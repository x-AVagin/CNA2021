
# Ce que j'obtient avec le code ASP qui minimize le nombre de pas de temps non expliqué
# ainsi que le nombre de litteraux
# quand les ts ont été dédupliqué pour les noeuds où il n'y avait pas de conflit dans la ts.
bnet = """
A, !B
B, I2
I1, A
I2, M
I3, M
L, !B
L_e, 1
M, I1
P, I3
"""
# %%
# ce que j'obtient si je vire les noeuds I1 I2 I3 ainsi que Le qui est constant
# qui était là juste pour marquer le délais pais qui soulent pour l'analyse
bnet = """
A, !B
B, M
L, !B
M, A
P, M
"""
# %%
# celui que j'aurai voulu avoir : tiré d'un chapitre de bouquin.
# il a 3 attracteurs
# mais JE PEUX PAS L'AVOIR, c'est sur que je ne peux pas l'avoir car L n'est pas son propre activateur quand on regarde les equa diff
bnet = """
A, A | (L & B)
B, M
L, P | (L & !B)
M, A
P, M
"""

# celui que je peux avoir si je me demerde:
bnet = """
A, A | (L & B)
B, M
L, Le & P
Le, 1
M, A
P, M
"""
