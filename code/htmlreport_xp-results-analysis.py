import argparse
import collections
import logging
import pathlib
import re
import sys

import matplotlib.pyplot as plt
import yaml

try:
    sys.path.insert(1, 'code/')
    import sbmlparser
    from reports import Report

except ImportError:
    print("is it the correct working directory ?")
    exit(1)
    # %pwd


def get_infos_FALSE_fn(path):
    nb_FALSE = 0
    nb_fn_tot = 0
    info_FALSE = set()
    for filepath in path.glob("**/*.bnet"):
        with open(filepath, 'r') as f:
            for line in f:
                nodeid, fn = line.split(",")
                nb_fn_tot += 1
                if fn.strip() == "0":
                    #            ---
                    # this is the way FALSE function is expressed in bnet format
                    nb_FALSE += 1
                    info_FALSE.add((filepath.parents[0], nodeid))

    return (nb_FALSE, nb_fn_tot, info_FALSE)


def get_fileswithpattern(pathglob, pattern):
    filelistwithpattern = []
    for file in pathglob:
        with open(file, 'r') as f:
            for line in f:
                if re.search(pattern, line):
                    filelistwithpattern.append(file)
                    continue
    return filelistwithpattern


def get_succeeded_pipeline(pathglob):
    return get_fileswithpattern(pathglob, "sm pipeline succeeded")


def get_failed_pipeline(pathglob):
    return get_fileswithpattern(pathglob, "sm pipeline failed")


if __name__ == '__main__':

    rep = Report()

    parser = argparse.ArgumentParser(
        description='Description',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )

    parser.add_argument('--xprespath',
                        help="""
                        Path to where the xp results are stored.
                        /!\ It assumes a certain folder structure. TODO : to document !!!
                        """,
                        type=pathlib.Path,
                        required=True
                        )

    args = parser.parse_args()

    # ======================================================================
    pattern_BIOMD = re.compile("(BIOMD\d{10})")

    processedSBMLs = []
    sbmllist_filepath = args.xprespath / "sbml-list.txt"

    with open(sbmllist_filepath, 'r') as f:
        for line in f:
            processedSBMLs.append(line.strip())

    # ======================================================================
    rep.add_title(f"Report for xp in {args.xprespath} ")

    rep.add_markdown("**sbml file that have been processed :**")

    for processedSBML in processedSBMLs:
        rep.add_markdown(processedSBML)
    rep.add_markdown(
        f"Nb of files to process = Nb of lines in sbml-list.txt: {len(processedSBMLs)}")

    # ======================================================================
    rep.add_markdown("**Distribution number of nodes in the sbml files**")
    distr_node_data = []
    taskdescrpath = args.xprespath / "taskdescr"
    for taskdescrfile in taskdescrpath.glob("BIOMD*.yaml"):
        with open(taskdescrfile, 'r') as f:
            content = yaml.load(f, Loader=yaml.FullLoader)
            distr_node_data.append(len(content["nodes"]))

    fig_histnodes = plt.hist(distr_node_data, bins='auto')
    plt.title(
        "Histogram of the number of nodes in the processed sbml files, with 'auto' bins")
    rep.add_figure()  # Add the fig that has just been plot
    plt.close()
    # ======================================================================
    pathsmlogs = args.xprespath / "logs"
    logsglob = pathsmlogs.glob("*.snakemake.log")

    smpip_succ = get_succeeded_pipeline(logsglob)
    rep.add_markdown(f"**nb succeeded pipeline :** {len(smpip_succ)}")
    rep.add_markdown(str(smpip_succ))

    smpip_fail = get_failed_pipeline(logsglob)
    rep.add_markdown(f"**nb failed pipeline :** {len(smpip_fail)}")
    rep.add_markdown(str(smpip_fail))

    equalitysign = "==" if (
        len(smpip_succ) + len(smpip_fail) == len(processedSBMLs)) else "!="
    rep.add_markdown(
        f"number pipeline that have succeeded + nb pip that have failed **{equalitysign}** total number of processed SBML")
    rep.add_markdown(
        f"{len(smpip_succ)} + {len(smpip_fail)} VS {len(processedSBMLs)}")

    # ======================================================================
    rep.add_markdown("___________________________________________")
    rep.add_title("About generated BN :", level=2)

    data_nb_generatedBN = []
    for processedSBML in processedSBMLs:
        rep.add_markdown(processedSBML)
        biomd = pattern_BIOMD.search(processedSBML)[0]

        # ----------
        # get number of nodes at total.
        # number of species species,
        # number of params modeified by rules

        libsbmlmodel = sbmlparser.sbmlfile2libsbmlmodel(processedSBML)

        nb_nodes = len(sbmlparser.getListOfNodes(
            libsbmlmodel, ["SBMLspecies", "SBMLparamsnotconstant"]))
        nb_species = len(sbmlparser.getListOfNodes(
            libsbmlmodel, ["SBMLspecies"]))
        nb_params = len(sbmlparser.getListOfNodes(
            libsbmlmodel, ["SBMLparamsnotconstant"]))
        rep.add_markdown(
            f"{nb_nodes} nodes = {nb_species} species + {nb_params} params")

        # ----------

        rep.add_external_figure(args.xprespath / "plots" / "dotfiles" /
                                f"{biomd}.svg")

        rep.add_external_figure(args.xprespath / "plots" / "ts" /
                                biomd / "tsraw.png")
        rep.add_external_figure(args.xprespath / "plots" / "ts" /
                                biomd / "tsstretched_notime.png")
        rep.add_external_figure(args.xprespath / "plots" / "ts" /
                                biomd / "histtsstretched_time.png")

        # nb of bnet found for this SBML :
        bnet_dirpath = args.xprespath / "bnet" / biomd
        bnet_glob = bnet_dirpath.glob("*.bnet")
        nb_bnet = len(list(bnet_glob))
        rep.add_markdown(
            f"{nb_bnet} BNs have been generated for this SBML file")
        data_nb_generatedBN.append(nb_bnet)

        # "{BIOMD_id}_stg_{semantics}.png"
        stg_glob = bnet_dirpath.glob("*stg*.png")

        # we sort the glob in order to add the images in the "correct" order
        list_stg_glob = list(stg_glob)
        list_stg_glob.sort()
        for stg_fileFullPath in list_stg_glob:

            rep.add_markdown(pathlib.Path(stg_fileFullPath).name)
            rep.add_external_figure(stg_fileFullPath)

    rep.add_title("distribution nb generated BNs :", level=2)

    fig_histnbgeneratedBNs = plt.hist(
        data_nb_generatedBN, bins='auto')

    data_nb_generatedBN.sort()
    print(data_nb_generatedBN)

    rep.add_markdown(str(data_nb_generatedBN))
    plt.title(
        "Histogram of the number of BNs generated per SBML file. 'auto' bins")
    rep.add_figure()  # Add the fig that has just been plot
    plt.close()

    # ======================================================================
    rep.add_markdown("___________________________________________")
    rep.add_title("About FALSE functions", level=2)
    bnetspath = args.xprespath / "bnet"
    nb_FALSE, nb_fn_tot, info_FALSE = get_infos_FALSE_fn(bnetspath)
    rep.add_markdown(
        f"{nb_FALSE} FALSE function has been found on the {nb_fn_tot} total number of function that have been generated. It is a rate of {nb_FALSE / nb_fn_tot}")

    info_FALSE_perbiomd = collections.defaultdict(set)
    for biomd, nodeid in info_FALSE:
        biomd = pattern_BIOMD.search(str(biomd))[0]
        info_FALSE_perbiomd[biomd].add(nodeid)

    for biomd in info_FALSE_perbiomd:
        rep.add_markdown(
            "**------------------------------------------------**")

        rep.add_markdown(f"**{biomd} : {info_FALSE_perbiomd[biomd]}**\n")

        rep.add_external_figure(args.xprespath / "plots" / "dotfiles" /
                                f"{biomd}.svg")
        rep.add_external_figure(args.xprespath / "plots" / "ts" /
                                biomd / "tsraw.png")
        rep.add_external_figure(args.xprespath / "plots" / "ts" /
                                biomd / "tsstretched_notime.png")
        rep.add_external_figure(args.xprespath / "plots" / "ts" /
                                biomd / "histtsstretched_time.png")

        for nodeid in info_FALSE_perbiomd[biomd]:
            rep.add_markdown("**------------**")

            rep.add_markdown(f"\n{nodeid}:\n")
            rep.add_external_figure(args.xprespath / "plots" / "dotfiles" /
                                    f"{biomd}_{nodeid.lower()}.svg")
            rep.add_external_figure(args.xprespath / "plots" / "ts" /
                                    biomd / f"histtsstretched_{nodeid.lower()}.png")
            rep.add_external_figure(args.xprespath / "plots" / "heatmaps_conflictingtimestep" /
                                    biomd / f"{nodeid.lower()}_heatmap_pbDynamic-location.png")

    print(f'report_{str(args.xprespath).replace("/", "-")}.html')
    rep.write_report(
        filename=f'report_{str(args.xprespath).replace("/", "-")}.html')
