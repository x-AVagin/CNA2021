
# %% sbml parser
model = sbmlparser.sbmlfile2libsbmlmodel(args.sbmlfilepath)

# %% general information about the model :
model.getNumSpecies()

logger.info(f"""----------------
    Information about the provided sbml :
    It has :
    - {len(model.getListOfFunctionDefinitions())} functions defined
    - {len(model.getListOfRules())} rules
    - {len(model.getListOfReactions())} reactions
    - {len(model.getListOfEvents())} events
    - {len(model.getListOfSpecies())} species at total
        - some are consided constant, namely :
            {[e.id for e in model.getListOfSpecies() if e.getConstant()]}
        - some are not consided constant, namely :
            {[e.id for e in model.getListOfSpecies() if not e.getConstant()]}
    - {len(listOf_RuleLeftSide)} variable (from left hand sides of the rules) namely :
    {listOf_RuleLeftSide}
    """)

   for function in model.getListOfFunctionDefinitions():
        logger.debug(function)

    for function in model.getListOfRules():
        logger.debug("---------")
        logger.debug(function)
        logger.debug(function.getName())
        logger.debug(function.getVariable())
        logger.debug(function.getFormula())
