import argparse
import collections
import pathlib
import sys
import textwrap

import matplotlib.pyplot as plt

try:
    sys.path.insert(1, 'code/')
    import sbmlparser
except ImportError:
    print("is it the correct working directory ?")
    # %pwd


def save_hist(data, name, filepath, nbbin=None):
    fig = plt.figure()
    ax = fig.add_subplot(111)
    if nbbin:
        ax.hist(data, nbbin, color="blue")
    else:  # automatic nbbin
        ax.hist(data, color="blue")
    ax.set_title(name)
    fig.tight_layout()
    fig.savefig(filepath, format='png')
    plt.close(fig)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Description', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--datafolder',
                        type=pathlib.Path,
                        required=True,
                        help=textwrap.dedent('''\
                        Path to the folder storing the sbml files on which we want to make some stats.
                        ''')
                        )
    parser.add_argument('--outfolder',
                        type=pathlib.Path,
                        required=True,
                        help=textwrap.dedent('''\
                        Path to the folder where to store the plots.
                        ''')
                        )

    args = parser.parse_args()
    # ------
    # False arguments
    # sys.argv = ["--datafolder", "data/BioModels/BioModels_Database-r31_pub-sbml_files/curated/", "--outfolder", "statsfolder"]
    # args = parser.parse_args(sys.argv)

    args.datafolder
    args.outfolder

    # count sbbml files :

    list_sbml = list(args.datafolder.glob("**/*.xml*"))
    len(list_sbml)

    print(f'{len(list_sbml)} sbml files in {args.datafolder}')

    # data is a dict
    # key is the BIOMD id,
    # contains a tuple :
    #   - nb of sbml species,
    #   - nb of sbml params
    #   - number of nodes taken into account by the pipeline = species + parameters,
    #   - number of sbml reaction
    #   - nb of sbml rules
    data = collections.defaultdict(tuple)
    data
    for sbmlfilepath in list_sbml:
        print(sbmlfilepath)
        model = sbmlparser.sbmlfile2libsbmlmodel(sbmlfilepath)

        # list of Nodes taken into account by the pipeline :
        # . retrive species (= metabs)
        listOfSpeciesTaken = {
            specie.id for specie in model.getListOfSpecies() if not specie.getConstant()}
        assert(model.getNumSpecies() == len(model.getListOfSpecies()))
        print(
            f"{len(listOfSpeciesTaken)} are taken i account on the {model.getNumSpecies()}")
        # . retrieve params (= values)
        listOfParamsTaken = {rule.getVariable()
                             for rule in model.getListOfRules()}
        assert(model.getNumParameters() == len(model.getListOfParameters()))
        print(
            f"{len(listOfParamsTaken)} are taken i account on the {model.getNumParameters()}")
        # . retrieve params (= values)

        # . merge the 2 lists
        listOfNodes = listOfSpeciesTaken.union(listOfParamsTaken)
        listOfNodes = [node.lower() for node in listOfNodes]
        len(listOfNodes)

        data[sbmlfilepath.stem] = (
            model.getNumSpecies(),
            model.getNumParameters(),
            len(listOfNodes),
            len(model.getListOfReactions()),
            len(model.getListOfRules())
        )

    data
    #   - nb of sbml species,
    #   - nb of sbml param
    #   - nb of nodes taken into account by the pipeline = species + parameters,
    #   - nb of sbml reaction
    #   - nb of sbml rules

    args.outfolder.mkdir(parents=True, exist_ok=True)

    # transform data :

    data_reshaped = zip(*data.values())
    # 5 lists of data :
    # data_nb_sbmlspecies, data_nb_sbmlparam, data_nbnodes, data_nb_sbmlreaction, data_nb_sbmlrules

    for data_, name in zip(data_reshaped, ["nb sbmlspecies", "nb sbmlparams", "nb nodes", "nb sbmlreactions", "nb sbmlrules"]):
        save_hist(data_, name, args.outfolder /
                  f'{name.replace(" ", "-")}.png', max(data_))
