import argparse
import logging
import os
import sys
import textwrap

import matplotlib.pyplot as plt

try:
    sys.path.insert(1, 'code/')
    import logger
    import tshandler
    logger.set_loggers()
except ImportError:
    print("is it the correct working directory ?")
    # %pwd


logger = logging.getLogger('logger.ts2plots')


def ts2lineplot(timecourse, file="", **kwargs):
    """
    Plot a time serie

    - line plots, with step drawstyle.
    - time in abscisse
    - concentration / activity in ordinate axis.
    - tight black box aroung the figure


    Arguments:
    ----------
    - timecourse (pandas dataframe) : the timeserie
        one col per agent

    - file (string) :
        path where to save the plot.
        if empty string is given, the plot will not be saved
        (but will still appears when lauching interactive python session)

    - **kwargs (optional):
        other arguments given to the plot function
        https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.plot.html
        - subplots : wheter of not put each col in a separate subplot
    """
    fig = timecourse.plot(kind="line", drawstyle="steps", **kwargs)

    #fig.get_figure().savefig(file, bbox_inches='tight')
    if file:
        plt.savefig(file, bbox_inches='tight')
        plt.close()
        # plt.close(fig.get_figure())

    else:
        logger.log("no filename has been given, thus the plot is not saved. ")
        return fig


def ts2histplot(timecourse, file="", **kwargs):
    """
    Plot an histogram of the values in the time serie

    - hist plot,
    - values in abscisse
    - density in ordinate axis.
    - tight black box aroung the figure


    Arguments:
    ----------
    - timecourse (pandas dataframe) : the timeserie
        one col per agent

    - file (string) :
        path where to save the plot.
        if empty string is given, the plot will not be saved
        (but will still appears when lauching interactive python session)

    - **kwargs (optional):
        other arguments given to the plot function
        https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.plot.html
        - subplots : wheter of not put each col in a separate subplot
    """

    fig = timecourse.hist(figsize=(20, 15))

    #fig.get_figure().savefig(file, bbox_inches='tight')
    if file:
        plt.savefig(file, bbox_inches='tight')
        plt.close()
        # plt.close(fig.get_figure())

    else:
        logger.log("no filename has been given, thus the plot is not saved. ")
        return fig


def plot_tsraw(timecourse, outplots_dirpath, prefix=""):

    ts2lineplot(timecourse,
                file=os.path.join(
                    outplots_dirpath, f"{prefix}tsraw.png"))
    ts2lineplot(timecourse,
                file=os.path.join(
                    outplots_dirpath, f"{prefix}tsraw_subplots.png"),
                subplots=True)
    ts2lineplot(timecourse.reset_index(drop=True),
                file=os.path.join(outplots_dirpath, f"{prefix}tsraw_notime.png"))


def plot_tsstretched(timecourse, outplots_dirpath, prefix=""):

    ts2lineplot(timecourse,
                file=os.path.join(outplots_dirpath, f"{prefix}tsstretched.png"))
    ts2lineplot(timecourse,
                file=os.path.join(
                    outplots_dirpath, f"{prefix}tsstretched_subplots.png"),
                subplots=True)
    ts2lineplot(timecourse.reset_index(drop=True),
                file=os.path.join(outplots_dirpath, f"{prefix}tsstretched_notime.png"))


def plot_histtsstretched(timecourse, outplots_dirpath, prefix=""):

    ts2histplot(timecourse.reset_index(drop=False),  # drop = False to plot distribution of time sampling
                os.path.join(outplots_dirpath, f"{prefix}histtsstretched.png"))

    for (columnName, columnData) in timecourse.iteritems():
        ts2histplot(columnData,
                    os.path.join(outplots_dirpath, f"{prefix}histtsstretched_{columnName}.png"))

    fig = plt.hist(timecourse.index, align="mid")
    plt.savefig(os.path.join(outplots_dirpath,
                             f"histtsstretched_time.png"), bbox_inches='tight')
    plt.close()


def plot_tsbinarized(timecourse_binarized, outplots_dirpath, prefix=""):

    ts2lineplot(timecourse_binarized,
                file=os.path.join(outplots_dirpath, f"{prefix}tsbinarized.png"))
    ts2lineplot(timecourse_binarized,
                file=os.path.join(outplots_dirpath,
                                  f"{prefix}tsbinarized_subplots.png"),
                subplots=True)
    ts2lineplot(timecourse_binarized.reset_index(drop=True),
                file=os.path.join(outplots_dirpath, f"{prefix}tsbinarized_notime.png"))
    ts2lineplot(timecourse_binarized.reset_index(drop=True),
                file=os.path.join(outplots_dirpath,
                                  f"{prefix}tsbinarized_notime_subplots.png"),
                subplots=True)


def plot_timecourse_binarized_subset_deduplicated(timecourse_binarized_subset_deduplicated, outplots_dirpath, prefix=""):

    ts2lineplot(timecourse_binarized_subset_deduplicated,
                file=os.path.join(outplots_dirpath, f"{prefix}timecourse_binarized_subset_deduplicated.png"))
    ts2lineplot(timecourse_binarized_subset_deduplicated,
                file=os.path.join(
                    outplots_dirpath, f"{prefix}timecourse_binarized_subset_deduplicated_subplots.png"),
                subplots=True)
    ts2lineplot(timecourse_binarized_subset_deduplicated.reset_index(drop=True),
                file=os.path.join(outplots_dirpath, f"{prefix}timecourse_binarized_subset_deduplicated_notime.png"))


def istoplot(plotname, argsplot):
    """
    Tells whether or not a plot should be made or not

    Arguments
    ---------
    - plotname : the name of the plot we wonder if it should be plotted or not
    - argsplot : the list of plots given with argparse

    Returns :
    ---------
    boolean depending if the plot should be printed or not.
    """
    possiblechoices = [
        'none', 'all', 'tsraw', 'tsstretched',
        'histtsstretched', 'tsbinarized'
    ]

    assert plotname in possiblechoices

    if argsplot == ['none']:
        logger.info("no plot have been asked")
        return False

    elif argsplot == ['all'] or plotname in argsplot:
        logger.info(f"{plotname} plot has been asked")
        return True

    else:
        # plotname not in argsplot
        logger.info(f"{plotname} plot has not been asked")
        return False


def argparse_str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


if __name__ == "__main__":

    # %% help message
    parser = argparse.ArgumentParser(
        description="""
        Produces plots from ts
        """,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    # script name, sbmlfilepath, res-simu.txt pknlpoutfilepath, plot option, optimization option, dirpath where to store frequency of values from the streatched ts

    parser.add_argument('--res_simu_fname',
                        type=str,
                        required=True,
                        help=textwrap.dedent('''\
                        csv file storing the result of the copasi simulation of the sbml file  being processed
                        ''')
                        )
    parser.add_argument('--plots',
                        type=str,
                        required=True,
                        nargs='+',  # 1 or more
                        choices=['none', 'all', 'tsraw', 'tsstretched',
                                 'histtsstretched', 'tsbinarized'],
                        help=textwrap.dedent('''\
                        Kind of graphic to plot
                        ''')
                        )
    parser.add_argument('--outplots_dirpath',
                        type=str,
                        required=False,
                        default="",
                        help=textwrap.dedent('''\
                        Directory where to store the plots
                        ''')
                        )

    parser.add_argument('--prefix_outfile',
                        type=str,
                        required=False,
                        default="",
                        help=textwrap.dedent('''\
                        Prefix to add the the plot file names.
                        ''')
                        )
    parser.add_argument("--globally",
                        type=argparse_str2bool,
                        nargs='?',
                        const=True,
                        required=True,
                        help=textwrap.dedent('''\
                            Whether to use the same strtching for all the components or not.
                            Default = False -> the stretching is done component wise, resulting in having a different binarization treshold for all the component in the binarisation step.
                        ''')
                        )

    # %% Retrieve arguments
    args = parser.parse_args()

    if any(i in args.plots for i in ["none", "all"]):
        assert len(args.plots) == 1  # "none" only OR "all" only

    if "none" not in args.plots:
        # some information are then required :
        assert(args.outplots_dirpath != "")
        os.makedirs(args.outplots_dirpath, exist_ok=True)

    # --------------
    if args.plots == ['none']:
        logger.info("no plots have been asked. Exiting...")
        sys.exit(0)

    logger.info(f"Outputs in '{args.outplots_dirpath}'")

    #########################################################################
    #########################################################################
    #########################################################################
    # Adapted from sbml2lp.py

    # -----------
    # ts raw :
    timecourse = tshandler.tsfile2pddf(args.res_simu_fname)

    if istoplot("tsraw", args.plots):
        plot_tsraw(timecourse,
                   args.outplots_dirpath,
                   args.prefix_outfile)

    # ---
    # Since ASP cannot use float, we map all the values as integer between -100 (representing 0) and 100 (representing 1)

    # streatching and intergerization
    timecourse = tshandler.ts2stretchedts(
        timecourse, minstretched=-100, maxstretched=100, globally=args.globally)

    logger.info("ts stretched")
    logger.info(timecourse.head())

    if istoplot("tsstretched", args.plots):
        plot_tsstretched(timecourse,
                         args.outplots_dirpath,
                         args.prefix_outfile)

    # plot ts value distributions
    if istoplot("histtsstretched", args.plots):
        plot_histtsstretched(timecourse,
                             args.outplots_dirpath,
                             args.prefix_outfile)

    # -----
    # binarization
    # Since the values are all map between -100 and 100, the binarization threasold is 0
    timecourse_binarized = tshandler.binarization(timecourse, treasold=0)
    logger.info("ts binarized to values -1 and 1")

    # ---
    if istoplot("tsbinarized", args.plots):
        plot_tsbinarized(timecourse_binarized,
                         args.outplots_dirpath,
                         args.prefix_outfile)
