
import argparse
import itertools
import logging
import os
import signal
import sys
from collections import defaultdict

import clyngor
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns

try:
    sys.path.insert(1, 'code/')
    import clyngoranswerparser
    import logger
    logger.set_loggers()
except ImportError:
    print("is it the correct working directory ?")
    exit(1)
    # %pwd

logger = logging.getLogger('logger.runsimu')


clyngor.load_clingo_module()
assert(clyngor.have_clingo_module())

# %% parse arguments
parser = argparse.ArgumentParser(
    description='Description', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('lpfiles', nargs='+',
                    help='Path of the knowledge.lp files. Should have one per agent.')


parser.add_argument('--stretchedtsfilepath',
                    help='Path to the file encoding the stretched ts as lp constraints (used only if the chosen optimization is "mae")',
                    type=str,
                    required=False
                    )
parser.add_argument('--binarizedtsfilepath',
                    help='Path to the file encoding the stretched ts as lp constraints (used only if the chosen optimization is "mae")',
                    type=str,
                    required=False
                    )

parser.add_argument("--outputdirpath",
                    help="where the bnet files should be outputed",
                    type=str,
                    )

parser.add_argument("--timeout",
                    help="How long (in seconds) will be alocated to clyngor solving",
                    type=int,
                    )
parser.add_argument("--nbcores",
                    help="Number of cores given to clingo",
                    type=int,
                    required=True
                    )
parser.add_argument("--optimization",
                    help='''
                        optimization option is either "mae" of "unexplainedbinary". (case insensitive).
                        if "mae", clingo has to minimize the mae between observed ts (stetched between -100 and 100) and binarized ts (using -100 to represent 0 and 100 to represent 1) .
                        if "unexplainedbinary", clingo will simply try to minimize the number of timestep it cannot explain in the (potentially deduplicated) binarized ts".
                            cannot explain = number of time step for which the parents has been observed at time t but despite this the child has not been activated at time t + 1.
                        ''',
                    choices=['mae', 'unexplainedbinary'],
                    type=str.lower
                    )
parser.add_argument("--nbmaxgeneratedBNs",
                    help='''
                        Number max of BNs generated by our approach.
                        (0 for all (default))
                        ''',
                    type=int,
                    required=True
                    )
parser.add_argument("--plotsdirpath",
                    help='''Path where to store the plots.
                    There is one plot per agent in the system.
                    This plot is an heatmap indicating where the counterexamples were.
                    ''',
                    type=str,
                    required=False
                    )

args = parser.parse_args()
# ----- fake arguments :
# print(sys.argv)
# sys.argv = [file for file in glob.glob("data/lp_BIOMD0000000111/knowledge_*.lp")] + ["--outputdirpath"] + ["test"] + ["--optimization"] + ["mae"] + ["--plotsdirpath"] + ["testplots/"]
# print(sys.argv)
# args = parser.parse_args(sys.argv)
# args
# ------
#

if args.plotsdirpath:
    os.makedirs(args.plotsdirpath, exist_ok=True)
    plot_option = True

if args.optimization == "mae":

    # I will need values stored in args.stretchedtsfilepath and args.binarizedtsfilepath
    # check if there :
    assert(args.stretchedtsfilepath and args.binarizedtsfilepath)

    clingooptimcode = 'code/minimization_MAE.lp'
    # clingo will minimize the MAE value obtained comparing the ts with binarized ts
    # the penality of each mistake is thus modulated by the importance of the mistake
    # Ex :
    # obs value | binarized value | boolean prediction  | penality
    # ----------+-----------------+---------------------+----------
    #    51            100               100               49
    #    70            100               100               30
    #    49           -100               100               51
    #     1           -100               100               99
elif args.optimization == "unexplainedbinary":
    clingooptimcode = 'code/minimization_numberofcounterexamples.lp'
    # only the binarized ts is taken into account, not the real observed values
    # clingo will minimize the number of timestep it could not explain in the binarized ts (potentially deduplicated)
    # the problem with this version is that all errors are penalized by 1,
    # the panality is not modulated by the actual error.
    # Ex :
    # obs value |binarized value | boolean prediction  | penality
    # ----------+-----------------+---------------------+----------
    #    51              1                1                 0
    #    70              1                1                 0
    #    49              1               -1                 1
    #     1              1               -1                 1


logger.debug("ici")
if args.timeout:
    logger.warning("timeout for clyngor set to {args.timeout}")
else:
    logger.warning("No timeout for clyngor has been set")

set_filespath = set()
for filepath in args.lpfiles:
    if os.path.isfile(filepath):
        set_filespath.add(filepath)
    else:
        logger.error(
            f"problem, (at least) one of the file does not exist : {filepath}")
        exit()

logger.debug("ici")


# %%
# To avoid eternal calls :

def handler_forever(signum, frame):
    print("Forever is over!")
    raise Exception("end of time")


# Register the signal function handler
signal.signal(signal.SIGALRM, handler_forever)


# %%

# where the data for a node are stored :
# lpfile = "data/lp_BIOMD0000000111/knowledge_cdc13t.lp"


# mode = "enumeration"
mode = "optimization"

if mode == "enumeration":
    logger.warning("enumeration mode")
    opt = ['--opt-mode=ignore']
    # '--quiet=0,0,0' (*** RuntimeError: In context '<libclingo>': unknown option: 'quiet' / on the cluster... :( )
elif mode == "optimization":
    logger.warning("optimization + enumeration mode")
    opt = ['--opt-mode=optN']
    # '--quiet=1,0,0' (*** RuntimeError: In context '<libclingo>': unknown option: 'quiet' / on the cluster... :( )

    # TODO : Remove non optimal models XXX
    # Information about how to quiet non optimal models :
    # https://sourceforge.net/p/potassco/mailman/message/34311933/
else:
    logger.warning("no mode defined")

opt.append(f"--parallel-mode={args.nbcores}")

bnet_constructor = defaultdict(list)

for lpfile in args.lpfiles:
    logger.info("-------------------------------------------")
    logger.info(lpfile)

    x = lpfile.split('knowledge_')[1].split('.lp')[0]
    logger.debug(x)

    answers = None
    try:
        # this may run too long... :/
        if args.timeout:
            signal.alarm(5)  # timeout
        logger.info("solving... ")

        # lp code list always contains (independently of if optimization is 'mae' or 'unexplainedbinary'):
        # - lp file
        # - identify.lp
        # - lp code handling the optimization
        lp_code_list = [lpfile, "code/identify.lp", clingooptimcode]
        # Then, if args.optimization == "mae", lp code list also contains :
        # - the ts-stretched.lp
        # - the binarized ts.lp
        if args.optimization == "mae":
            lp_code_list.extend(
                [args.stretchedtsfilepath,
                 args.binarizedtsfilepath])

        # clyngor.solve returns a clyngor.answers.ClingoAnswers object *yielding* answer sets
        answers = clyngor.solve(
            lp_code_list,
            # clingooptimcode depends on the argument "optimization"
            options=opt,
            use_clingo_module=True,
            # raise an ASPWarning when encountering a clingo warning:
            error_on_warning=True,
            # print full command to stdout before running it:
            print_command=True
        ).by_arity
        # use answears.by_arity property to group atoms by predicate and arity.
        # Answer sets will then be dict with predicate/arity as keys and collection of args as value
        # See more in clyngor code here :
        # https://github.com/Aluriak/clyngor/blob/c0f94c3535120ac64bd6283ee921b5cc25403bc7/clyngor/answers.py#L130

        if args.timeout:
            # cancel the timer, in case the call finished before timeout:
            signal.alarm(0)
    except Exception as exc:
        # If something went wrong, exit.

        logger.error(
            f"Trouble during ASP solving (probably). lpfile was {lpfile}")
        logger.info(exc)

        if not answers:
            logger.error(
                f"""
                No answers have been generated.
                """)
        else:
            logger.error(
                f"""
                Still, they were answers generated. Answers = {answers}
                """
            )
        logger.error("Quitting")
        exit(1)

    # answers = clyngor.solve(["test-pbDynamiccounterexample.lp"])
    logger.info("setting the solving is over")

    # use clyngor.opt_models_from_clyngor_answers
    # to return tuple of optimal models found by clingor.solve from answers.
    # https://github.com/Aluriak/clyngor/blob/3794a646d2b2bf8068a27fd5ab01318bc23838bf/clyngor/utils.py#L283
    # /!\ this function assumes that
    # - Option '--opt-mode=optN' have been given to clingo.
    # - that models are yielded by clingo in increasing optimization value,
    # therefore there is no need to verify that a model B succeeding a model A
    # is better if they have different optimization value.
    if "--opt-mode=optN" in opt:
        answers = clyngor.opt_models_from_clyngor_answers(answers)
        print("Taking opt answers")

    num_answers = 0

    nb_cube_predicats = []

    # list of lists storing, for each AS, where were the time step where a pb in the dynamic was spotted
    pbDynamic_heatmapData = []

    # list of MAE values
    if args.optimization == "mae":
        answers_MAE = []

    print("entering for loop")
    for idx, answer in enumerate(answers):
        logger.info(f"processing answer {idx}")

        num_answers += 1

        answerSympy = clyngoranswerparser.clyngorAnswer2sympy(
            answer, by_arity=True)
        logger.info(answerSympy)

        bnet_constructor[x].append(
            clyngoranswerparser.bfSympy2bfbnet(answerSympy)
        )

        if args.optimization == "mae":
            logger.info("retrieving MAE")
            answers_MAE.append(
                clyngoranswerparser.clyngorAnswer2mae(answer, by_arity=True))

        nb_cube_predicats.append(
            clyngoranswerparser.clyngorAnswer2numberofcubes(answer, by_arity=True))

        list_TimestepsErrors_with_pbDynamic = clyngoranswerparser.clyngorAnswer_retrivebypredicate(
            answer,
            "error",
            by_arity=True
        )
        pbDynamic_heatmapData.append(list_TimestepsErrors_with_pbDynamic)

    if num_answers == 0:
        logger.error(
            "Problem. No answer was produced. The problem was unsat. Quitting...")
        sys.exit(1)

    else:
        logger.info(
            f"{num_answers} models have been found")
        logger.info(
            f"the number of cubes in these answers is ranging from {min(nb_cube_predicats)} to {max(nb_cube_predicats)} \n {nb_cube_predicats}")
        if args.optimization == "mae":
            # assert optimization was effective by ensuring there is only one MAE value
            # that is the same for all the AS
            assert(len(set(answers_MAE)) == 1)
            logger.info(f"Their MAE value is {answers_MAE[0]}")

    if plot_option:
        print(answer)
        nbtime = clyngoranswerparser.clyngorAnswer2nbtime(
            answer, by_arity=True)

        logger.info("saving data pf the heatmap about error")
        heatmap_table = np.zeros((num_answers, nbtime), dtype=int)
        # heatmap_table = np.zeros( (3, 101), dtype=int)
        for answer_index, answer_pbDynamic in enumerate(pbDynamic_heatmapData):
            for (time_index, error_value) in answer_pbDynamic:
                logger.debug(answer_index, time_index, error_value)
                heatmap_table[answer_index, time_index] = error_value

        np.savetxt(os.path.join(args.plotsdirpath,
                                f"{x}_heatmapdata_pbDynamic-location.csv"),
                   heatmap_table,
                   delimiter=',',
                   # do not use scientific notation, but pure integer :
                   fmt='%d'
                   )

        logger.info(f"ploting hm for {x}")
        # ---
        # create colormap :
        # The max of the error = | obsvalue - threasold | = | obsvalue - 0 | = | obsvalue | is 100
        cmap = mpl.colors.LinearSegmentedColormap.from_list(
            'Custom cmap', ["white", "red"], 100)

        heatmap_plot = sns.heatmap(heatmap_table,
                                   # remove line between square of the heatmap otherwise, we have the impression the
                                   linewidths=0,
                                   # use the column names but plot only every 10 labels :
                                   xticklabels=10,
                                   # Values to anchor the colormap.
                                   # They could have been inferred from the data
                                   # but let's specify it to be sure heatmaps are comparable.
                                   vmin=0,
                                   vmax=100,
                                   # color map
                                   cmap=cmap
                                   )
        heatmap_plot.tick_params(labelsize=5)
        fig = heatmap_plot.get_figure()
        fig.savefig(os.path.join(args.plotsdirpath,
                                 f"{x}_heatmap_pbDynamic-location.png"))
        plt.close(fig)
        logger.info(
            f'heatmap created is {os.path.join(args.plotsdirpath,f"{x}_heatmap_pbDynamic-location.png")}')

logger.debug(bnet_constructor)

# ----------------------
# %% Create a BN in PyBoolNet

agents = sorted(bnet_constructor)

# print list of bnet_possibilities :
logger.debug(list(itertools.product(*(bnet_constructor[x] for x in agents))))

# ----------------------
# %% Store BN in bnet files.

os.makedirs(args.outputdirpath, exist_ok=True)

# bnet_constructor if a dict
#   keys are the nodes of the system
#   values are list of boolean function

# The following magic produces all the possible BN.
# by creation all the possible association of all the different booleans function of the agents.
# One bnet possibility is a list of boolean expression
# This list is sorted by `agents` which is the sorted list of nodes in the system
for i, bnet_possibility in enumerate(itertools.product(*(bnet_constructor[x] for x in agents))):
    if i >= args.nbmaxgeneratedBNs:
        break
    logger.debug("----------")
    logger.debug(bnet_possibility)

    bnet = ''
    for agent, expression in zip(agents, bnet_possibility):
        bnet += f"{agent}, {expression}\n"

    logger.debug(bnet)

    with open(os.path.join(args.outputdirpath, f"{i+1}.bnet"), "w") as fout:
        # i+1 for the file name to start at 1 instead of 0 (-> it is consistent with the numerotation of BNs outputed by RBoolNet REVEAL & Best-Fit)
        fout.write(bnet)
