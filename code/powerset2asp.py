#!/usr/bin/env python3

import itertools
import string
from itertools import chain, combinations
from typing import List, Set, Union


def get_powerset(l, save_mem=True):
    # type:(Union[List[int], List[str]])->Union[List[Set[str]], List[Set[int]]]
    """ Compute powerset of a set

    Argument:
    ----------
    - l : a list-like of n element with which to construct the powersets
    Returns :
    - save_mem : Boolean (default True)
    ----------
    a generator (if save_mem) or a list (if not save_mem) of a powerset
    contains 2^n element.

    Example :
    ---------
    >>> get_powerset(["a", "b", "c"], False)
    [set(),
     {'a'},
     {'b'},
     {'c'},
     {'a', 'b'},
     {'a', 'c'},
     {'b', 'c'},
     {'a', 'b', 'c'}]
    """
    # inpired from recipe in itertool documentation :
    # https://docs.python.org/2/library/itertools.html
    # powerset([1,2,3]) --> () (1,) (2,) (3,) (1,2) (1,3) (2,3) (1,2,3)
    chained = chain.from_iterable(combinations(l, r)
                                  for r in range(len(l) + 1))
    if save_mem:  # return the generator
        return chained

    # else : return a list
    powersets = [set(e) for e in chained]
    assert(len(powersets) == 2**len(l))
    return powersets


def is_contradictive(s: Set[str]) -> bool:
    """ Say if a set is contradictive.
    == contains both a litteral and its opposite
    Positive literal are represented by upper case letter
    Negative litteral are represented by lower case letter

    Argument :
    -----------
    s : a set of string (litterals are represented by a letter)
    Returns :
    ----------
    boolean is the given set is contradictive

    Examples :
    -----------
    >>> is_contradictive(set(["a", "A"]))
    True
    >>> is_contradictive(set(["a", "b"]))
    False
    """
    for e in s:
        if (e.upper() in s and e.lower() in s):
            return True
    return False


if __name__ == '__main__':
    n = 2

    atoms = list(itertools.chain(
        *[(string.ascii_uppercase[i], string.ascii_lowercase[i])
          for i in range(0, n)]))

    atoms
    ps = get_powerset(atoms)
    len(ps)
    # remove contradictive :
    ps = [s for s in ps if not is_contradictive(s)]
    ps
    len(ps)

    # with open("powersets_" + str(n), "w"):
    for combiID, combi in enumerate(ps):
        print(f"nbInput({combiID}, {len(combi)}) .")
        for a in [string.ascii_uppercase[i] for i in range(0, n)]:
            if a.upper() in combi:
                val = 1
            elif a.lower() in combi:
                val = -1
            else:
                val = 0
            print(f"possibleClause({combiID}, {a.lower()}, {val}) .")
