# setup logging
# snipet and explanation from http://sametmax.com/ecrire-des-logs-en-python/
import logging
from logging.handlers import RotatingFileHandler


def set_loggers(levelstring="info"):
    """
    Sets a logger with 2 handlers :
    - a file handler
    - a redirection to the console

    The logger can then be retrived in a module after having import logger.py :

    ```
    try:
        sys.path.insert(1, 'code/')
        import logger
        logger.set_loggers(levelstring)
    except ImportError:
        print("is it the correct working directory ?")
        # %pwd

    logger = logging.getLogger('logger.id')
    ```


    Argument :
    ----------
    - levelstring : str  (not case sensitive)
        DEBUG or INFO (the default) or WARNING or ERROR or CRITICAL

    """

    numeric_level = getattr(logging, levelstring.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError('Invalid log level: %s' % levelstring)

    # create the logger objet that is used to write in the logs
    logger = logging.getLogger("logger")

    # levels hierachy : CRITICAL > ERROR > WARNING > INFO > DEBUG
    # -> setting the level to DEBUG makes it write everything.
    logger.setLevel(numeric_level)

    # The formator adds time and level or each message written in the log
    formatter = logging.Formatter(
        '%(asctime)s :: %(name)s :: %(levelname)s :: %(message)s')

    # handlers :
    # - One handler will redirect the log in a file.
    # mode : write , 1 backup, max size = 1Mo
    file_handler = RotatingFileHandler(
        'activity.log', 'w', 1000000, 1)
    file_handler.setLevel(numeric_level)
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)

    # - A second handler redirects log on the console?
    stream_handler = logging.StreamHandler()
    stream_handler.setLevel(numeric_level)
    logger.addHandler(stream_handler)

    logger = logging.getLogger("logger.loggerinit")
    logger.info('logger is set')
