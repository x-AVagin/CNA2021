import argparse
import logging
import pathlib
import random
import sys

import numpy as np
from matplotlib import pyplot as plt

try:
    sys.path.insert(1, 'code/')
    import logger
    import pknhandler
    logger.set_loggers()
    logger = logging.getLogger('logger')

except ImportError:
    print("is it the correct working directory ?")
    # %pwd


parser = argparse.ArgumentParser(
    description='Description', formatter_class=argparse.ArgumentDefaultsHelpFormatter)

parser.add_argument('--pknfolder',
                    type=pathlib.Path,
                    required=True,
                    help=textwrap.dedent('''\
                    Where are the pkn stored
                    ''')
                    )

args = parser.parse_args()


data = []
# list of tuples : (BIOMDID, nodeID, # incoming edge)

# open each pkn sif file.
for siffilepath in args.pknfolder.glob('*.sif'):
# for siffilepath in pathlib.Path("/home/nanis/Documents/TRAVAIL/THESE/PROJETS/CMSB2020/experiments/CMSB2021-results_merge-all_1to10/pkn/").glob('*.sif'):
# for siffilepath in pathlib.Path("/data2/avaginay/bm2bn/experiments/CNA2021_rerunall_lessthan10incomingedges/pkn/").glob('*.sif'):
for siffilepath in pathlib.Path("experiments/CNA2021_rerunall_lessthan10incomingedges/pkn/").glob('*.sif'):
    print(siffilepath)

    sifstruct = pknhandler.siffile2sifstruct(siffilepath)
    # list of list : [ "nodeInput", value (1 or -1), "nodeOutput" ]

    # assert no dupies in this list :
    # sifstruct_set = set(tuple(l) for l in sifstruct)
    # need to convert the inner lists to tuples so they are hashable

    # convert to sifstruct2linksstruct:
    linkstruct = pknhandler.sifstruct2linksstruct(sifstruct)
    # dict {y : [(x, sign), (x, sign)]}
    # describing x--(sign)--> Y
    # example : {'x': {('a', 1), ('b', '-1')}, 'a': set(), 'b': set()}

    # compute the number of input edge.
    for nodechild in linkstruct:
        print(nodechild)
        data.append(
            (
                siffilepath,
                nodechild,
                len(linkstruct[nodechild])
            )
        )

# sort data by number of incoming edges
data.sort(key=lambda a: a[2])
# show the 5 bigest :
data[-5:]

datatoplot = [v for (b, n, v) in data

[val for (BIOMD, node, val) in data if BIOMD == pathlib.Path(
    "/data2/avaginay/bm2bn/experiments/CNA2021_rerunall_lessthan10incomingedges/pkn/BIOMD0000000640.sif")]

# print histogram

plt.close("all")
plt.hist(datatoplot, bins=max(datatoplot) - min(datatoplot))
plt.title('title')
plt.xlabel('# incoming edges')
plt.ylabel('count')

plt.show()

plt.savefig('histograme_distrib_incomingedges_CNA21.png')


# print boxplot.

plt.close("all")
plt.boxplot(datatoplot)
plt.title('title')
plt.xlabel('# incoming edges')
plt.ylabel('count')

plt.show()
plt.savefig('boxplot_distrib_incomingedges_CNA21.png')



###########

data[1:5]

#

np.median(datatoplot)

X = 10


biomodelswithmorethanX= set(biomd for biomd, _, v in data if v > X)
print(len(biomodelswithmorethanX))


biomodelswithlessthanX= set(biomd for biomd, _, v in data) - biomodelswithmorethanX
print(len(biomodelswithlessthanX))


tot= len(biomodelswithmorethanX) + len(biomodelswithlessthanX)
print(tot)


print(f"{len(biomodelswithlessthanX) / tot * 100} % ( = {len(biomodelswithlessthanX)}) biomodels with less than {X} incoming edges")


# Save list to file :
# with open("sbmlList/sbmlList_CNA2021")
