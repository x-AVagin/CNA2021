
import argparse
import logging
import sys
import textwrap

try:
    sys.path.insert(1, 'code/')
    import pknhandler
    import sbmlparser
except ImportError:
    print("is it the correct working directory ?")
    # %pwd


logger = logging.getLogger('logger.sbml2dot')


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description='extract influences from sbml. Store them as dot file', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--sbmlfilepath',
                        type=str,
                        required=True,
                        help=textwrap.dedent('''\
                        Paht to the sbml file we want to extract the influences from
                        ''')
                        )
    parser.add_argument('--nodesTypes',
                        required=True,
                        nargs='+',
                        help=textwrap.dedent('''\
                        List of nodeTypes that are to take into account while determining the influences.
                        TODO : take into account all the nodesTypes defined in the function sbmlparser.getListOfNodes
                        ''')
                        )
    parser.add_argument('--dotfilepath',
                        type=str,
                        required=True,
                        help=textwrap.dedent('''\
                        Path of the dot file we will write the PKN in.
                        ''')
                        )

    args = parser.parse_args()

    logger = logging.getLogger('logger.sbml2dot')

    logger.info(
        f"""
        Given sbml is {args.sbmlfilepath}.
        The dot file in which to write is {args.dotfilepath}.
        {len(args.nodesTypes)} types of nodes are taken into account, namely :
        {args.nodesTypes}

        Let's construct the PKN using the nodes from these nodeTypes
        """)

    # %% sbml parser
    model = sbmlparser.sbmlfile2libsbmlmodel(args.sbmlfilepath)

    # Assert same node ID is not found to have several nodesTypes
    # Because if it is the case, the code to retrieve influences might be wrong.
    try:
        assert(sbmlparser.pairwise_disjoint_nodesTypes(
            model, args.nodesTypes))
    except:
        logger.error(
            f"""At least a node has been found to belong to everal nodesTypes
            among the ones provided {args.nodesTypes}.
            Unfortunately, this could mess up with finding influences,
            since the same entity could be labelled differently...
            Have to exit, sorry.
            """)
        exit(1)

    # Retrieve influences concerning the nodes taken into account :
    # nodeA < relationship type : +1 ou -1 > nodeB
    listOfNodesTaken = sbmlparser.getListOfNodes(
        model, args.nodesTypes, safe=True)
    influences = sbmlparser.get_influences(
        model,
        listOfNodesTaken,
        sanitize=True
    )

    logger.info("---------")
    logger.info("Influences have been retrieved")
    logger.debug(influences)

    # %% output a DOT file :
    pknhandler.sifstruct2dotfile(
        influences, args.dotfilepath, listOfNodes=listOfNodesTaken)
    logger.info(f"dot file has been written in {args.dotfilepath}")
