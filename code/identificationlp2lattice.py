import collections
import imp
import itertools
import os
import string

import clyngor

from sympy import *

os.getcwd()


python_code_file = open(
    "code/ASPencoding_version20200326/preview-lattice_overingeeeering.py")
bn2lattice = imp.load_module(
    'python_code', python_code_file, './python-code', ('.py', 'U', 1))


n = 2
atoms = list(itertools.chain(
    *[(string.ascii_uppercase[i], string.ascii_lowercase[i])
      for i in range(0, n)]))
ps = get_powerset(atoms)
len(ps)
# remove self contradictive :
ps = [s for s in ps if not is_contradictive(s)]
len(ps)

answers = clyngor.solve('code/ASPencoding_version20200326/identify.lp')
answers = list(answers)
len(answers)
dfiles = []
for answer_idx, answer in enumerate(answers):
    print(answer_idx)
    psclauseanswear = collections.defaultdict(set)
    for e in answer:
        print(e)
        predicate = e[0]
        if predicate == "clause":
            clauseID, nodeID, val = e[1]
            print(clauseID, nodeID, val)
            if val != 0:  # mean tha the node is really in the clause :
                psclauseanswear[clauseID].add(
                    nodeID.upper() if val == 1 else nodeID.lower())
    colored_ps = [e for e in ps if e in psclauseanswear.values()]
    d = lattice2dot(f"out/truc{answer_idx}.gv", ps, colored_ps)
    d.render(view=False)
    print(d)
    d
    dfiles.append(d)


dfiles
for f in dfiles:
    f
