
import sys
from math import trunc
from random import randint

import clyngor
import numpy as np
from sklearn.metrics import mean_absolute_error

import matplotlib.pyplot as plt

sys.version
print(sys.executable)


def test_clyngorusingclingo():
    import clyngor
    assert(clyngor.have_clingo_module())


def test_maeSklearn_vs_maeASP():
    """
    Assert the MAE found by ASP is the same than the MAE found by sklearn

    Random y_true between -100 and 100
    int because float are not handled in ASP

    Random y_pred :
    either -100 or 100

    Note that in our setup, -100 represent the 0
    """

    y_true = [randint(-100, 100) for i in range(5)]
    y_pred = np.random.choice([-100, 100], size=len(y_true))

    y_true
    y_pred

    x = range(len(y_true))

    plt.plot(x, y_true, label="y_true")
    plt.plot(x, y_pred, label="y_pred", drawstyle="steps")
    plt.xlabel('time')
    plt.ylabel('y')
    plt.title('y_true and y_pred over time')
    plt.legend()
    plt.show()

    maeSklearn = mean_absolute_error(y_true, y_pred)
    maeSklearn

    plt.plot(x, abs(y_true - y_pred), drawstyle="steps", label="error")
    plt.xlabel('time')
    plt.ylabel('error')
    plt.axhline(y=maeSklearn, color='r', linestyle='-', label="MAE")
    plt.title('error |y_true - y_pred|')
    plt.legend()
    plt.show()

    ASPcode = ""
    ASPcode += "nbtime(NbT) :- NbT=#count {time(T) : time(T) }."

    for t, (o, ohat) in enumerate(zip(y_true, y_pred)):
        print(t, (o, ohat), (abs(o - ohat)))
        ASPcode += (f"time({t}) . obs({t}, {int(o)}). obsBinarized({t}, {int(ohat)}).\n")

    ASPcode

    ASPcode += "error(T,  | O - Ohat |) :- obs(T, O);  obsBinarized(T, Ohat) ; time(T).\n"

    ASPcode += "mae(S / (NbT)) :- S = #sum{ E, T : error(T, E), time (T) } ; nbtime(NbT).\n"
    # /!\ syntax in the sum is so it potentially takes duplicated values into account.
    # this would happen if two error are the same for two different timesteps.

    ASPcode += "#show.\n"
    ASPcode += "#show error/2."
    ASPcode += "#show mae/1.\n"

    ASPcode
    answer = clyngor.solve(inline=ASPcode)
    answer = list(answer)
    # answears are list of frozenset (one per AS). Each predicate is a tuple (predicated name, (tuple of values))
    answer
    assert len(answer) == 1

    for p, v in answer[0]:
        print(p, v)
        if p == "mae":
            maeASP, = v

    trunc(maeSklearn), maeASP
    assert(trunc(maeSklearn) == maeASP)
