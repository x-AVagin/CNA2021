
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Instead of just minimazing the number of time step identified with to the predicate :
%   `pbDynamic_countrerexample(N, T)`
% we now want to weight these time steps with an error measure.
% This error measure is given by the MAE computed is computed between ts and the threasold.
% Indeed :
% If the continuous ts value was just below the binarizing threasold, it will be binarized as -1
% but this 0-value is not that clear.
% Thus the penalty associated to the error at this time step should be smaller than the penalty associated
% with a time step where the binary value was also -1 but the continuous ts value was -100.



% error at time T is the absolute value of the difference between obs and the threasold (which is 0, so we can discard the minus operation)
% error = |obsvalue - zero| = | obsvalue |
% At the end, ASP try to optimize putting the problematic time step is the region of the ts were the absolute value of the obs stretched between -100 and 100 is small
% Remember we compute the error **only for timestep that have been found to be counterexamples**

%error(T, | O |) :- obs(T, x, O); pbDynamic(T) .


% When a pbdynamic appears at time T, the error for this time is the average of the error of the timesteps on which the erroneous configuration spans.
% -> determine on which timesteps the configurations are spanning:
continuousduplicate_tuple(T, Tfirst) :- continuousduplicate(T) ; time(T); time(Tfirst);  Tfirst < T ; not continuousduplicate(Tfirst) ;
   continuousduplicate(Ta):time(Ta), Ta > Tfirst, Ta <= T  .

% T is the continuous duplicates of itself:
continuousduplicate_tuple(T, T) :- time(T).
%continuousduplicate_tuple(T, T) :- continuousduplicate(T+1) ; time(T).
%continuousduplicate_tuple(T, T) :- .

% in case of pdDynamic at time T, determine the error:
error(T, S/C):- S=#sum{|V|, Tother: continuousduplicate_tuple(Tother, T) , obs(Tother, x, V) } ;  C=#count{Tother: continuousduplicate_tuple(Tother, T) , obs(Tother, x, V)}  ; pbDynamic(T).
% for each pbdynamics at T, the error is the sum of all values on which the configuration happening at T, divided by the number of timestep on which the config is spanning.




%error(0, 0) . % to avoid S being unsafe if no errors.

% MAE :
sumErrors(S) :- S = #sum{ E, T : error(T, E) } .

% This should make the solving faster in the cases sumErrors ins 0 for a constant candidate (most parsimonious)
% because ASP will not check the less parsimonious candidates.
% sumErrors(S) >= 0 .

% Note :
% with this other way to compute the error, it does not work better :
%error(T, NodeID, | O |) :- obs(T, x, O); pbDynamic_countrerexample(C, T) ; clause(C, NodeID, _).
%sumErrors(S) :- S = #sum{ E, NodeID, T : error(T, NodeID, E) } .
%#show error/3.

%   mae corresponds to the average of the errors spotted in the timeserie,
%   thus the sum of errors divided by the number of timesteps
%
% - for child node without parents :
%   clauseID(0) is chosen by default.
%   so pbDynamic_countrerexample will be generated will be generated at each time
%   but since there is no information about obs and binarizedObs,
%   no predicate error will be generated
%   so sumErrors will be automatically 0
%   so their mae will be 0
% - for child node with parent but for which no pbDynamic_countrerexample have been spotted -> found a Boolean expression explaining perfectly the dynamic :D :D :D
%   no pbDynamic_countrerexample,
%   so no error predicated generated
%   so sumError will be 0
%   so their mae will be 0
% - for child node with parents and pbDynamic_countrerexample spotted :
%   the sum of error will not be 0.


mae(S / NbT) :- sumErrors(S) ; nbtime(NbT).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% minimization :
% priority is 2 -> more priority than 1 (which is about the number of inputs)
#minimize{S@2 : sumErrors(S)}.
%   Since ASP does not handle floats, it seems better to minimize sumErrors than MAE
%   Example : if sumErrors = 42, but nbTime=50, MAE = sumErrors / nbtime will be considered to be 0.
%   Since the answer set also minimize the number of inputs, simpler functions with a bigger sumError but the same final MAE will be prefered while not realy be the best ones !!.
% #minimize{M@2 : mae(M)}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#show mae/1.
%#show obs/3.
% on genere une erreur fictive pour eviter un keyErreur dans le cas où aucune erreur n'est generée :
error(0,0).
#show error/2.
#show sumErrors / 1.
%#show pbDynamic/1.
