

# to rerun the experiments on biomodels sbml:
xpconfig="xpconfig_CNA21_rerunall.yml"

#############################################
# get the sbml file processed :
snakemake -s CNA2021.smk sbmlList2foldersbml --cores 1 --reason -p --configfile $xpconfig

# get all the taskdesc files
snakemake -s CNA2021.smk taskdescr --cores 1 --reason -p --configfile $xpconfig

# get all the TS:
snakemake -s CNA2021.smk all_simucopasi --cores 50 --reason -p --configfile $xpconfig

# get all the PKN: 
snakemake -s CNA2021.smk all_pknsif --cores 50  --reason -p --configfile $xpconfig

# BN synthesis :
snakemake -s CNA2021.smk all_BNsidentification --cores 50  --reason -p --configfile $xpconfig #-n --touch

# identification for a SBML in particular:
#cp data/BioModels/BioModels_Database-r31_pub-sbml_files/curated/BIOMD0000000065.xml experiments/xpname/sbmlfiles/BIOMD0000000065.xml
#snakemake -s CNA2021.smk experiments/xpname/bns/BIOMD0000000065/our/identification.done --cores 1  --reason -p --configfile $xpconfig


#----
# compute the coverages: 
snakemake -s CNA2021.smk all_get_coverage_stgVSts --cores 5  --reason -p --configfile $xpconfig -n




# --- Get table number of BNs and coverage results : 
snakemake -s CNA2021.smk aggregation_results_table --cores 1  -p --configfile $xpconfig
python3 code/aggregation_results_image_CNA2021.py --aggregation_results_table experiments/CNA2021_rerunall_lessthan10incomingedges/results/aggregation_resultats_table_mixed.csv --pattern 'BIOMD' --jitter_x 0.3 --jitter_y 0 --hists True --identificationmethods our --plotY median --plotX maxnbinputs --pathout experiments/CNA2021_rerunall_lessthan10incomingedges/results/aggregation_results_jitterx0.3jittery0.png


#snakemake -s CNA2021.smk all_aggregationresults --cores 1  -p --configfile $xpconfig -n
# experiments/<xpname>/results/number_generatedBNs_table.csv
# experiments/<xpname>/results/aggregation_resultats_table_mixed.csv


# aggregation coverage image : 
# python3 code/aggregation_results_image_CNA2021.py --aggregation_results_table experiments/CNA2021_rerunall_lessthan10incomingedges/results/aggregation_resultats_table_mixed.csv --pattern 'BIOMD' --jitter_x 0.3 --jitter_y 0 --hists True --identificationmethods our --plotY median --plotX maxnbinputs --pathout experiments/CNA2021_rerunall_lessthan10incomingedges/results/aggregation_results_jitterx0.3jittery0.png

 
 
 
snakemake -s CNA2021.smk all_aggregation_results_image --cores 1  -p --configfile $xpconfig -n

# snakemake -s CNA2021.smk cumulative_plot_runtime --cores 1  -p --configfile $xpconfig -n

snakemake -s CNA2021.smk boxplot_runtime --cores 1  -p --configfile $xpconfig -n






#############################################################

# Running Example paper :

snakemake -s CNA2021.smk runningexample_paperCNA2021_sbmlList --cores 7 --reason -p --configfile xpconfig_CMSB2021_runningexample.yml  --notemp

snakemake -s CNA2021.smk sbmlList2foldersbml --cores 1 --reason -p --configfile xpconfig_CMSB2021_runningexample.yml
snakemake -s CNA2021.smk taskdescr --cores 1  --reason -p --configfile xpconfig_CMSB2021_runningexample.yml
snakemake -s CNA2021.smk all_simucopasi --cores 50   --reason -p --configfile xpconfig_CMSB2021_runningexample.yml
snakemake -s CNA2021.smk all_pknsif --cores 50  --reason -p --configfile xpconfig_CMSB2021_runningexample.yml
snakemake -s CNA2021.smk all_BNsidentification --cores 50 --reason -p --configfile xpconfig_CMSB2021_runningexample.yml -n
snakemake -s CNA2021.smk all_get_coverage_stgVSts --cores 1   --reason -p --configfile xpconfig_CMSB2021_runningexample.yml -n
snakemake -s CNA2021.smk all_aggregation_results_image --cores 1   -p --configfile xpconfig_CMSB2021_runningexample.yml -n


